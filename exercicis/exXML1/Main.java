package exXML1;

import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class Main {

	public static void main(String[] args) throws IOException {

		List<Persona> persones = new ArrayList<Persona>();
		persones.add(new Persona("Jordi", 22));
		persones.add(new Persona("Marta", 32));
		persones.add(new Persona("Joan", 19));
		persones.add(new Persona("Pere", 30));
		persones.add(new Persona("Maria", 21));

		StringBuilder b = null;
		long pos = 0;
		String nom;
		int edat;

		try (RandomAccessFile fitxer = new RandomAccessFile("persones.bin", "rw")) {
			for (int i = 0; i < persones.size(); i++) {
				b = new StringBuilder(persones.get(i).getNom());
				b.setLength(20); // Asigna mida de 20 caracters al contingut de
									// StringBuilder
				fitxer.writeChars(b.toString()); // nom -----> 2*20bytes=40bytes
				fitxer.writeInt(persones.get(i).getEdat()); // edat -----> int
															// (4 bytes)
															// total per
															// persona: 44 bytes
			} // Total: 44 bytes * 5 persona = 220 bytes
		
		
			

			try(FileWriter writer = new FileWriter("persones.xml")) {
				// per accedir a un registre multipliquem per la mida de
				// cada registre.
				
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document document = builder.newDocument();
				document.setXmlVersion("1.0");
				Element elementPersones = document.createElement("persones");
				document.appendChild(elementPersones);

				for (int i = 0; i < persones.size(); i++) {
					if (pos < 0 || pos >= fitxer.length())
						throw new IOException("N�mero de registre inv�lid.");
					pos = i * 44;

					// nostrem la informacio del registre triat
					fitxer.seek(pos);
					nom = readChars(fitxer, 20);
					edat = fitxer.readInt();

					Element elementPersona = document.createElement("persona");
					elementPersona.setAttribute("id", Integer.toString(i));
					document.getDocumentElement().appendChild(elementPersona);

					Element elementEdat = document.createElement("edat");
					Element elementNom = document.createElement("nom");
					Text text = document.createTextNode(nom);
					elementNom.appendChild(text);
					elementPersona.appendChild(elementNom);
					text = document.createTextNode(Integer.toString(edat));
					elementEdat.appendChild(text);
					elementPersona.appendChild(elementEdat);
				}

				Source source = new DOMSource(document);
				Result result = new StreamResult(writer);

				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "5");
				transformer.transform(source, result);

				Result console = new StreamResult(System.out);
				transformer.transform(source, console);
			} catch (ParserConfigurationException | TransformerException | IOException e) {
				System.err.println(e.getMessage());
			}
		} catch (IOException ex){
			
		}
		
		LectorDOMObjectes.llegeixMascotes(new ArrayList<Persona>());
		ConversorXSL.pasaXSL();
		LectorSAX.llegeixSAX();
		
	}

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i = 0; i < nChars; i++) {
			ch = fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}
	
	

}
