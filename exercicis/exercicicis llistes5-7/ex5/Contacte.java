package ex5;

public class Contacte implements Comparable<Contacte> {
	
	private String nom;
	private String cognom;
	private String adreca;
	private String telefon;
	
	public Contacte(String nom, String cognom, String adreca, String telefon){
	
		this.nom = nom;
		this.cognom = cognom;
		this.adreca = adreca;
		this.telefon = telefon;
	}

	@Override
	public int compareTo(Contacte c) {
		
		int resultat = String.CASE_INSENSITIVE_ORDER.compare(this.cognom, c.cognom);
		if(resultat == 0)
			resultat = String.CASE_INSENSITIVE_ORDER.compare(this.nom, c.nom);
		
		return resultat; 
	}

	public String getNom() {
		return nom;
	}

	public String getCognom() {
		return cognom;
	}

	public String getAdreca() {
		return adreca;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	@Override
	public String toString(){
		return "Nom: "+this.getNom()+"\nCognom: "+this.getCognom()+"\nAdre�a: "+this.getAdreca()+"\nTelefon: "+this.getTelefon();
				
	}			
				
				
	
	
}
