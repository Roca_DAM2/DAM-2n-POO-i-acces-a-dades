import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Jardi j1 = new Jardi(25);
		j1.plantaLlavor(new Altibus(), 2);
		j1.plantaLlavor(new Declinus(), 18);
		j1.plantaLlavor(new Declinus(), 10);
		j1.plantaLlavor(new Llavor(new Declinus()), 4);
		
		Scanner sc = new Scanner(System.in);
		String sortir;
	
		
		
		do{
			System.out.println("el teu jardi:");
			System.out.format(j1.toString());
			j1.temps();
			System.out.println("\n 's' per sortir :");
			sortir = sc.nextLine();
		}while(!(sortir.equals("s")));
		
		System.out.println("adeu");
		sc.close();
	}
	

}
