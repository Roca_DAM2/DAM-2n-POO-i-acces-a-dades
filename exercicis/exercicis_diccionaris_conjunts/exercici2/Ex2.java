package exercici2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.List;;

public class Ex2 {

	public static void main(String[] args) {
		
		List<Integer> llista = new ArrayList<Integer>();
		Set<Integer> conjunt = new HashSet<Integer>();
		Scanner sc = new Scanner(System.in);
		Integer num1;
		Integer num2;
		
		System.out.println("Introdueix un n�mero:");
		num1 = sc.nextInt();
		System.out.println("Introdueix un n�mero:");
		num2 = sc.nextInt();
		
		
		//Imprimir nom�s els n�meros que son divisibles per els dos
		//n�meros introduits
		for(int i=2; i<=1000; i++)
			if(num1 % i == 0 && num2 % i == 0)
				conjunt.add(i);
		
		llista.addAll(conjunt);
		Collections.sort(llista);
		for(int num : llista)
			System.out.println(num);
		
		conjunt.removeAll(conjunt);
		llista.removeAll(llista);
		
		
		//Nombres del 2 al 1000 que siguin divisors del primer nombre 
		//o del segon (sense repetir nombres).
		System.out.println("");
		
		for(int i=2; i<=1000; i++)
			if(num1 % i == 0 || num2 % i == 0)
				conjunt.add(i);	
		
		llista.addAll(conjunt);
		Collections.sort(llista);
		for(int num : llista)
			System.out.println(num);
		
		conjunt.removeAll(conjunt);
		llista.removeAll(llista);
		//Nombres del 2 al 100 que no siguin divisor ni del primer nombre 
		//ni del segon (sense repetir nombres).
		
		System.out.println("");
		
		for(int i=2; i<=100; i++)
			if(num1 % i != 0 && num2 % i != 0)
				conjunt.add(i);
		
		
		llista.addAll(conjunt);
		Collections.sort(llista);
		for(int num : llista)
			System.out.println(num);
		
		conjunt.removeAll(conjunt);
		llista.removeAll(llista);
		
		sc.close();
	}

}
