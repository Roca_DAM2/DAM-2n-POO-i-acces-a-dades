package ex1Text;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class comptarA {
	
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix la ruta d'un fitxer:");
		String nomFitxer = sc.next();
		
		int lectura;
		char ch;
		int cont = 0;
		
	
		
		try (FileReader reader = new FileReader(nomFitxer)){
			while((lectura = reader.read()) != -1){
				ch = (char) lectura;
				if (ch == 'a' || ch == 'A'){
					cont++;
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("Numero de lletres 'a': "+cont);
		
		sc.close();
	}

}
