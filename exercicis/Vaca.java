package zoo;

public class Vaca extends Animal {

	@Override
	public String mou(Zoo zoo) {
		return "Una vaca es posa a dormir";
	}

	@Override
	public String alimenta(Zoo zoo) {
		return "Una vaca comen�a a pastar amb tranquilitat";
	}

	@Override
	public String expressa(Zoo zoo) {
		return "Una vaca fa mu";
	}

}
