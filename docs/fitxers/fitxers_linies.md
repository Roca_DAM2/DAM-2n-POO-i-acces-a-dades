## Lectura i escriptura de fitxers de text línia a línia

Encara que *FileWriter* ja permet escriure cadenes senceres a un fitxer,
internament treballa caràcter a caràcter. *FileReader* directament no permet
llegir línies de text.

Per tal de treballar línia a línia tenim les classes *BufferedWriter* i
*BufferedReader*. A més de facilitar-nos el treball, aquestes classes
guarden els caràcters a escriure en un *buffer* de memòria, i després els
guarda tots de cop al fitxer. D'aquesta forma es redueixen la quantitat
d'accessos a disc que necessitem i s'accelera notablement l'execució del
programa.

### Escriptura de fitxers línia a línia

El següent exemple llegeix línies de text de teclat i crea un fitxer amb
aquestes línies de text.

Per indicar que ja hem introduït totes les dades que volem enviarem un
senyal de final de fitxer. A GNU/Linux aquest senyal s'envia amb Ctrl+D i
a Windows amb Ctrl+Z.

El programa rep un paràmetre, que és el nom del fitxer a escriure:

```java
public class EscriptorLinies {
	public static void main(String[] args) {
		String s;

		if (args.length == 1) {
			String nomFitxer = args[0];
			try (Scanner entrada = new Scanner(System.in);
					BufferedWriter escriptor = new BufferedWriter(new FileWriter(nomFitxer))) {
				System.out.println("");
				while (entrada.hasNextLine()) {
					s = entrada.nextLine();
					escriptor.write(s);
					escriptor.newLine();
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		} else {
			System.err.println("Ús: java EscriptorLinies <fitxer>");
		}
	}
}
```

- Hem utilitzat el mètode *hasNextLine()* per saber si podíem llegir més
línies de teclat o si ja hem rebut el final de fitxer.

- Hem utilitzat el mètode *newLine()* de *BufferedWriter* per escriure un salt
de línia adequat a la plataforma on s'executa el programa.

- És interessant veure que el constructor de *BufferedWriter* rep com a
paràmetre un *FileWriter*. Internament, el *BufferedWriter* utilitzarà els
mètodes de *FileWriter* per escriure al fitxer, modificant la forma de fer-ho
en la seva implementació. Aquest esquema es coneix amb el nom de *Decorator
Pattern*.

### Lectura de fitxers línia a línia

El següent exemple utilitza un *BufferedReader* per llegir un fitxer qualsevol
línia a línia i mostrar-lo per pantalla.

```java
public class LectorLinies {
	public static void main(String[] args) {
		String s;

		if (args.length == 1) {
			try (BufferedReader lector = new BufferedReader(new FileReader(args[0]))) {
				while ((s=lector.readLine()) != null)
					System.out.println(s);
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		} else {
			System.err.println("Ús: java LectorLinies <fitxer>");
		}
	}
}
```

- *readLine()* retorna *null* quan hem arribat a final de fitxer.

- *readLine()* considera que una línia acaba quan troba qualsevol de les
marques que s'utilitzen per indicar final de línia: \\n, \\r o \\r\\n.

### Comparativa de velocitat entre *FileWriter* i *BufferedWriter*

En aquest programa comparem la velocitat d'execució entre un *FileWriter* i
un *BufferedWriter*.

Aprofitant que tots dos deriven de *Writer* hem creat un mètode que rep un
*Writer* qualsevol i l'utilitza per escriure una línia 10 milions de cops.

Amb *System.currentTimeMillis()* obtenim el temps en el moment en què el
cridem. Mirant el temps abans de començar i just quan acabem el procés, i
restant els dos temps, podem saber quan ha tardat a fer l'operació.

```java
public class TestVelocitatBuffer {

	public static void main(String[] args) {		
		try (BufferedWriter bufferedWriter =
				new BufferedWriter(new FileWriter("provaBuffered.txt"))) {
			long temps = testVelocitat(bufferedWriter);
			System.out.println("provaBuffered.txt creat en " + temps + " ms");
		} catch (IOException e) {
			System.err.println(e);
		}

		try (FileWriter fileWriter = new FileWriter("provaFile.txt")) {
			long temps = testVelocitat(fileWriter);
			System.out.println("provaFile.txt creat en " + temps + " ms");
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public static long testVelocitat(Writer writer) throws IOException {
		long tempsInicial=System.currentTimeMillis();
		for(int i = 0; i < 10000000; i++) {
			writer.write("prova");
			writer.write('\n');
		}
		long tempsFinal=System.currentTimeMillis();
		return tempsFinal-tempsInicial;
	}
}
```

El resultat d'executar el programa és alguna cosa similar a:

```
provaBuffered.txt creat en 1375 ms
provaFile.txt creat en 3244 ms
```

Això significa que un *BufferedWriter* és més del doble de ràpid que un
*FileWriter*.
