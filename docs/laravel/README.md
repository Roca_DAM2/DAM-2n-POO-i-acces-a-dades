**POO i accés a dades**

M6UF4. Components d'accés a dades
=================================

Laravel
-------

- [Instal·lació](installacio.md)
- [Migracions i llavors de bases de dades](migracions.md)
- [Enrutament](rutes.md)
- [Vistes](vistes.md)
- [Controladors](controladors.md)
- [Accés a una base de dades amb Eloquent](eloquent.md)
- [Mapeig de relacions amb Eloquent](relacions.md)
