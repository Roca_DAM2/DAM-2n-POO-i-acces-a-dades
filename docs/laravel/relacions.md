## Mapeig de relacions amb Eloquent

Un altre aspecte important en la gestió d'una base de dades són les relacions
entre les taules.

L'Eloquent ens permet definir les relacions a les classes que fan de model,
de manera que l'accés a dades relacionades (*claus forànies*) se simplifica.

Per establir una relació entre dos models afegirem un mètode a cadascun d'ells.
La forma de crear aquests mètodes depèn del tipus de relació que hi hagi
entre les dues taules.

### Relacions ú a ú

#### Declaració

Són les relacions més simples possibles. Imaginem que cada usuari està
relacionat amb una adreça, i una adreça està relacionada només amb un usuari.
A nivell de base de dades, l'usuari guarda la clau fornània d'adreça.

Per definir aquesta relació al model ho faríem així:

```php5
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  public function address() {
      return $this->hasOne('App\Address');
  }
}
```

```php5
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
  public function user() {
      return $this->belongsTo('App\User');
  }
}
```

En aquests exemples i els següents s'està suposant que les claus forànies
són sempre de la forma *taula_id*. Per exemple, a *users* hi hauria un camp
anomenat *address_id* que és la clau forània de *addresses*. Aquest
comportament es pot modificar si cal.

#### Ús

Podem accedir a un model associat en una relació directament com si es
tractés d'un atribut més:

```php5
$address = User::find(1)->address;
```

En aquest exemple obtenim l'objecte *Address* associat a l'usuari amb *id* 1.

### Relacions ú a molts

#### Declaració

Exemplifiquem aquest tipus de relació amb les nostres classes *Plant* i
*Garden*: una planta està sempre a un jardí, i a un jardí hi ha moltes plantes.

A nivell de base de dades tenim una clau forània provinent de *gardens* a
la taula *plants*.

```php5
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Garden extends Model
{
  public function plants() {
      return $this->hasMany('App\Plant');
  }
}
```

La relació inversa:

```php5
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
  public function garden() {
      return $this->belongsTo('App\Garden');
  }
}
```

#### ÚS

Podem obtenir tots els models associats a un element directament:

```php5
$plants = Garden::find(1)->plants;
foreach ($plants as $plant) {
  //
}
```

Aquí hem obtingut una col·lecció de totes les plantes que te el jardí amb
identificador 1.

També podem fer una consulta basant-nos en els elements associats, de forma
que no els obtinguem tots sinó només els que compleixin alguna condició:

```php5
$plants = Garden::find(1)->plants()->where('water', '<', 4)->get();
```

La diferència entre aquest últim exemple i l'anterior és l'ús de parèntesis
al cridar l'associació *plants*.

Si no utilitzem els parèntesis obtenim els objectes directament, en aquest
cas les plantes. Si utilitzem els parèntesis obtenim la consulta que estem
construïnt, cosa que ens permet afegir-li qualsevol clàusula. Com que estem
treballant amb una consulta, al final hem de cridat *get()* per obtenir els
objectes seleccionats.

Com hem fet amb les relacions ú a ú, podem obtenir directament el jardí al
qual pertany una planta:

```php5
$gardenName = Plant::find(1)->garden->name;
```

### Relacions molts a molts

#### Declaració

Imaginem que les nostres plantes poden tenir diverses malalties i, naturalment,
una malaltia pot afectar a diverses plantes.

Quan creem el model relacional d'aquesta situació acabem amb tres taules:
la taula *plants*, la taula *diseases* i la taula *plant_disease*. Aquesta
última taula guarda una clau forània a cadascuna de les taules que
està relacionant.

En els models d'Eloquent no crearem la taula intermitja, sinó que treballarem
només amb les classes *Plant* i *Disease*:

```php5
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
  public function diseases() {
    return $this->belongsToMany('App\Disease');
  }
}
```

```php5
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
  public function plants() {
    return $this->belongsToMany('App\Plant');
  }
}
```

Imaginem ara que, a més, la gravetat amb què una malaltia afecta a una planta
la codifiquem amb un nombre enter anomenat *severity* a la taula intermitja
*plant_disease*.

Ara, per poder accedir a aquest camp, la definició de les relacions queda
una mica modificada:

```php5
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
  public function diseases() {
    return $this->belongsToMany('App\Disease')->withPivot('severity');
  }
}
```

```php5
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
  public function plants() {
    return $this->belongsToMany('App\Plant')->withPivot('severity');
  }
}
```

#### Ús

Treballem de la mateixa manera que hem fet en la relació ú a molts. Podem
obtenir directament tots els objectes relacionats amb un objecte que tinguem:

```php5
$plant = Plant::find(1);
foreach ($plant->diseases as $disease) {
  //
}
```

Aquí hem recuperat totes les malalties que afecten a una certa planta.

També podem treballar en format de consulta, afegint un parèntesis després
del nom de la relació.

Per exemple, en aquest exemple comptabilitzem la quantitat de malalties que
té una planta:

```php5
$nDiseases = $plant->diseases()->count();
```

Si la nostra relació té atributs, els podem recuperar utilitzant el nom
de propietat especial *pivot*.

En aquest exemple, comprovem si una planta té alguna malaltia amb un nivell
d'afectació superior a 80:

```php5
$plant = Plant::find(1);
foreach ($plant->diseases as $disease) {
  if ($disease->pivot->severity > 80) {
    $plant->die();
  }
}
```

### Càrrega impacient

Analitzem el següent exemple:

```php5
$gardens = Garden::all();

foreach ($gardens as $garden) {
  echo $garden->player->name;
}
```

En aquest codi s'està fent una consulta primer, per obtenir tots els jardins
que hi ha a la base de dades, i després s'està fent una consulta més per a cada
jardí, per obtenir el jugador propietari de cadascun d'ells.

Si tenim, per exemple, 100 jardins, això implica fer un total de 101 consultes.

Però és innecessari fer tantes consultes: quan recuperem els jardins ja sabem
que a continuació necessitarem els seus propietaris, així que podríem fer
una consulta adaptada que ja ens retornés tota aquesta informació d'un sol cop.

Eloquent ens permet indicar quines dades s'han de recuperar quan demanem un
model a la base de dades. El següent codi és funcionalment idèntic a l'anterior
però resol el problema utilitzant només dues consultes contra la base de dades:

```php5
$gardens = Garden::with('player')->get();

foreach ($gardens as $garden) {
  echo $garden->player->name;
}
```

A la primera consulta es recuperen tots els jardins, i a la segona es recuperen
tots els jugadors propietaris d'algun dels jardins anteiors.

Amb aquesta sintaxi podem especificar totes les relacions que s'han de recuperar
immediatament, cosa que pot millorar molt el rendiment de la nostra
aplicació.

Podem demanar més d'una relació, com en el següent codi, en què recuperem
els jugadors propietaris dels jardins i totes les plantes que hi ha a cada
jardí:

```php5
$gardens = Garden::with('player', 'plants')->get();
```

O podem encadenar relacions, com en aquest altre exemple, en què de cada
jardí recuperem les seves plantes, i de cada planta l'espècie a la qual
pertany:

```php5
$garden = Garden::with('plants.species');
```

### Inserció de models relacionats

Quan guardem a una base de dades una fila que està relacionada amb altres
files hem de completar els camps que contenen les claus forànies amb les
claus primàries de les files relacionades.

Podem fer això utilitzant Eloquent, sense haver de consultar i guardar a mà
els identificadors de cada fila associada.

En aquest primer exemple guardem una nova planta i l'associem a un jardí
existent:

```php5
$plant = new Plant();
$garden = Garden::find(1);
$garden->plants()->save($plant);
```

Per fer-ho, hem agafat el jardí i la seva relació amb plantes i hem cridat
el mètode *save()* passant-li la nova planta. Aquesta crida ha guardat la
planta a la base de dades i li ha assignat la clau forània del jardí.

Imaginem ara que volem fer una operació molt similar, però la planta que
volem associar a un jardí ja està guardada a la base de dades.

En aquest podem utilitzar el mètode *associate()* sobre la relació entre
planta i jardí, de manera que la planta queda associada al jardí que li
passem per paràmetre. Com que aquesta és una relació ú a molts, la planta ja
queda automàticament dissociada del jardí on estigués abans.

Finalment, per fer efectius els canvis que hem fet a l'objecte a la base de
dades, cridem el seu mètode *save()*:

```php5
$plant = Plant::find(1);
$garden = Garden::find(1);
$plant->garden()->associate($garden);
$plant->save();
```

Amb això ja tenim els transplantaments de plantes resols. Però potser hem de
traslladar la planta primera, de manera que passa un temps entre que
l'arrenquem d'un jardí i la plantem a un altre. El codi següent simplement
elimina l'associació existent entre una planta i el seu jardí, sense
assignar-li un jardí nou:

```php5
$plant->garden()->dissociate();
$plant->save();
```

Una altra qüestió són les relacions molts a molts. Aquí una fila no s'associa
a una única fila de l'altre taula, sinó que el què fem és afegir una fila
nova a la taula intermitja, possiblement amb algun atribut que pugui tenir
la relació.

Com abans, per fer l'associació a través d'Eloquent hem de recuperar la
relació primer i després cridar el mètode adequat, que en aquest cas és
*attach()*:

```php5
$disease = Disease::find(1);
$plant = Plant::find(1);
$plant->diseases()->attach($disease, ['severity'=>5]);
```

*attach()* rep un paràmetre obligatori que és l'objecte que es vol associar.
A més, podem passar-li un segon paràmetre amb un array associatiu que
s'utilitzarà per omplir els atributs de la relació a la taula pivot.

També podem eliminar l'associació entre dos files:

```php5
$plant->diseases()->detach($diseaseId);
```

O actualitzar els valors de la relació a la taula pivot:

```php5
$plant->diseases()->updateExistingPivot($diseaseId, ['severity'=>10]);
```
