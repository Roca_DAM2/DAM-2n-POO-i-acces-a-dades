
public class Altibus extends Planta{
	
	
	
	@Override
	Llavor creix(){	
		super.creix();
		if (altura > 7)
			return new Llavor(new Altibus());
		else
			return null;
	}
	
	@Override
	public char getChar(int nivell){
		if(altura > nivell){
			return '|';
		}
		else 
			if (nivell ==  altura){
			return 'O';
		}
		else
			return super.getChar(nivell);
	}
}
