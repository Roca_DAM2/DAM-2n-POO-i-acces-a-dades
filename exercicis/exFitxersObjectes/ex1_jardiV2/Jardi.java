package jardiV2;

import java.io.Serializable;

/**
 * La classe Jardi gestiona els elements d'un jard� i permet
 * veure'n l'evoluci� a mida que passa el temps.
 */
public class Jardi implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Constant que defineix l'al�ada m�xima d'un jard�
	 */
	private static final int ALTURA_JARDI = 10;
	/**
	 * Vector amb tots els elements del jard�
	 */
	private ElementJardi[] elements;
	/**
	 * Per facilitar passar el jard� a cadena, aqu� es guarda
	 * la l�nia de terra del jard�.
	 */
	private String terra = "";
	/**
	 * Constructor sense par�metres. Crea un jard� de mida 10.
	 */
	public Jardi() {
		this(10);
	}
	/**
	 * Constructor amb un par�metre. Crea un jard� amb la mida especificada.
	 * 
	 * @param mida  Mida que ha de tenir el jard�. Ha de ser un nombre positiu.
	 */
	public Jardi(int mida) {
		/*
		 * Si la mida �s incorrecta, creem un jard� de mida 10 i mostrem un error
		 * per pantalla. M�s endavant veurem com �s millor tractar aquestes
		 * situacions utilitzant excepcions.
		 */
		if (mida <= 0) {
			System.err.println("Jardi: mida incorrecta");
			mida = 10;
		}
		elements = new ElementJardi[mida];
		for (int i = 0; i < mida; i++)
			terra += "_";
	}

	/**
	 * Fa passar el temps per aquest jard�.
	 */
	public void temps() {
		ElementJardi nouElement;
        ElementJardi element;
		int i;
		int lloc;
		
		for (i = 0; i < elements.length; i++) {
			if (elements[i] != null) {
                element = elements[i];
				// fem passar el temps per tots els elements
				nouElement = element.temps();
				// esborrem els elements morts
				if (!element.isViva())
					elements[i] = null;
				// si s'ha creat un nou element, l'intentem posar
				if (nouElement != null) {
					lloc = i + element.escampa();
					posaElement(nouElement, lloc);
				}
			}
		}
	}

	/**
	 * Retorna una cadena multil�nia amb una representaci� del jard�.
	 * 
	 * @return  Una cadena que representa tot el jard�.
	 */
	@Override
	public String toString() {
		int altura, posicio;
		String s = "";
		
		// per crear la cadena comencem per l'al�ada m�xim i anem baixant
		for (altura = ALTURA_JARDI - 1; altura >= 0; altura--) {
			// per a cada element del jard�...
			for (posicio = 0; posicio < elements.length; posicio++) {
				if (elements[posicio] == null)
					s += ' ';
				else
					// ... afegim el car�cter que correspon a l'al�ada actual.
					s += elements[posicio].getChar(altura);
			}
			// salta de l�nia al final de cada al�ada
			s += "\n";
		}
		// a baix de tot afegim la l�nia de terra
		s += terra;
		return s;
	}

	/**
	 * Intenta posar un nou element al jard�.
	 * 
	 * @param nouElement  Element que es vol posar al jard�
	 * @param pos  Posici� en la qual es vol posar el nou element.
	 * @return  true si s'ha pogut posar l'element, false si no. Un element no
	 * es pot posar si la posici� indicada no existia al jard�, o si existia per�
	 * ja estava ocupada per un altre element.
	 */
	public boolean posaElement(ElementJardi nouElement, int pos) {
		boolean hiHaLloc = false;
		if (pos >= 0 && pos < elements.length && elements[pos] == null) {
			hiHaLloc = true;
			elements[pos] = nouElement;
		}
		return hiHaLloc;
	}
}