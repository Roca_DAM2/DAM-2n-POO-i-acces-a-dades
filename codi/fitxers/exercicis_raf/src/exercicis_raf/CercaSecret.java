package exercicis_raf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class CercaSecret {
	private static final int MIDA_REGISTRE = 10; // int (4) + 3 char (2)

	public static void trobaCodi(String nomFitxer, int codi) {
		long nRegistres;
		long regBaix;
		long regAlt;
		long regMitja;
		int codiLlegit;
		String secret;
		boolean trobat = false;

		try (RandomAccessFile raf = new RandomAccessFile(nomFitxer, "rw")) {
			nRegistres = raf.length() / MIDA_REGISTRE;
			regBaix = 0;
			regAlt = nRegistres - 1;
			// calculem registre a accedir
			while (regBaix <= regAlt && !trobat) {
				regMitja = (regBaix + regAlt) / 2;
				// comprovem registre
				raf.seek(regMitja * MIDA_REGISTRE);
				codiLlegit = raf.readInt();
				if (codiLlegit == codi) {
					secret = "" + raf.readChar() + raf.readChar() + raf.readChar();
					trobat = true;
					System.out.println("El secret corresponent al codi " + codi + " és " + secret);
				} else if (codiLlegit > codi) {
					regAlt = regMitja - 1;
				} else { // codiLlegit < codi
					regBaix = regMitja + 1;
				}
			}
			if (!trobat)
				System.out.println("No s'ha trobat el codi " + codi);
		} catch (FileNotFoundException e) {
			System.err.println("No s'ha pogut obrir el fitxer: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Error accedint al fitxer: " + e.getMessage());
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Quin codi vols cercar?");
		try {
			String sNum = sc.nextLine();
			trobaCodi("secret.bin", Integer.parseInt(sNum));
		} catch (NumberFormatException e) {
			System.err.println("Cal introduir un nombre");
		}
		sc.close();
	}
}
