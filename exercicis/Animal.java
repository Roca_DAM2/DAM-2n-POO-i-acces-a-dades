package zoo;

import java.util.Random;

public abstract class Animal implements Esser{
	
	private final Random rand = new Random();
	
	public abstract String mou(Zoo zoo);
	public abstract String alimenta(Zoo zoo);
	public abstract String expressa (Zoo zoo);
	
	@Override
	public String accio(Zoo zoo){
		
		int opcio = rand.nextInt(3);
		String resultat = "";
		
		switch (opcio){
			case 0:
				resultat = this.mou(zoo);
				break;
			case 1:
				resultat = this.alimenta(zoo);
				break;
			case 2:
				resultat = this.expressa(zoo);
				break;
		}
		 return resultat;
	}
}
