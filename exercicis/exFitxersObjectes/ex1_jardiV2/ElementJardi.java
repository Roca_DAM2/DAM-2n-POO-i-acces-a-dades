package jardiV2;

/**
 * Aquesta interf�cie defineix els m�todes m�nims que ha de tenir qualsevol
 * element del jard�. Fixeu-vos com gr�cies a l'�s d'aquesta interf�cie
 * hem aconseguit que Jardi no depengui de cap classe, com Planta o Llavor.
 */
public interface ElementJardi {
	/**
	 * Aquest m�tode retorna el car�cter que s'ha de printar per aquest element
	 * a l'altura especificada. Per exemple, per una planta, es retornaria el
	 * car�cter corresponent a la flor si es demana la mateixa altura que
	 * l'al�ada de la planta. Si l'altura demanada �s menor, es retornaria el
	 * car�cter corresponent a la tija, i si �s major, es retornaria un espai.
	 * 
	 * @param altura  Altura per la qual es vol saber el car�cter a escriure.
	 * @return  El car�cter que s'ha d'escriu a l'altura especificada per aquest
	 * element del jard�.
	 */
	public char getChar(int altura);
	/**
	 * El m�tode temps es crida cada vegada que es necessita que aquest element
	 * actu� en el jard�. En general, en aquest m�tode les plantes creixeran,
	 * i potser moriran. Si l'element genera un altre element, aquest s'haur�
	 * de retornar.
	 * 
	 * @return  Un nou element que s'hagi creat, o null si no s'ha creat cap
	 * element.
	 */
	public ElementJardi temps();
	/**
	 * Retorna si aquest element ha de seguir al jard� o no.
	 * 
	 * @return  true si es vol que aquest element segueixi al jard�, false si
	 * s'ha de retirar.
	 */
	public boolean isViva();
	/**
	 * Si aquest element en crea un altre, es pot cridar aquest m�tode per saber
	 * a quina posici� s'hauria de situar, respecte a la posici� que ocupa aquest
	 * element. Per exemple, si el nou element ha d'anar a la mateixa posici�,
	 * aquest m�tode retornaria 0. Si retorna 1, significa que el nou element va
	 * una posici� cap a la dreta d'aquest.
	 * 
	 * @return  La quantitat d'espais on situar un nou element al jard� respecte
	 * a la posici� d'aquest element.
	 */
	public int escampa();
}
