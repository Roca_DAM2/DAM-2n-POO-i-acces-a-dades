package zoo;

import java.util.Scanner;

public class PrincipalZoo {
	
	static Zoo zoo = new Zoo();
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		String comanda = "";
			
		System.out.println("El zoo obre les seves portes:");
		while (!(comanda.equals("surt"))){
			System.out.println("introdueix una acci� (\"surt\" per sortir) :");
			comanda	= sc.nextLine();
			switch (comanda){
				case "afegeix vaca":{
					zoo.afegeixAnimal("vaca");
					System.out.println("Ha arribat una  enorme vaca al zoo");
					break;
				}
				case "afegeix cocodril":{
					zoo.afegeixAnimal("cocodril");
					System.out.println("Ha arribat un perill�s cocodril al zoo");
					break;
				}
				case "treure vaca":{
					zoo.suprimeixAnimal("vaca");
					System.out.println("S'ha traslladat una vaca a un altre zoo");
					break;
				}
				case "treure cocodril":{
					zoo.suprimeixAnimal("cocodril");
					System.out.println("S'ha traslladat un cocodril a un altre zoo");
					break;
				}
				case "treure totes les vaques":{
					zoo.suprimeixTots("vaca");
					System.out.println("El zoo deixa de tenir vaques");
					break;
				}
				case "treure tots els cocodrils":{
					zoo.suprimeixTots("cocodril");
					System.out.println("El zoo deixa de tenir cocodrils");
					break;
				}
				case "mira":{
					zoo.mira();
					break;
				}
				case "mostra animals":{
					System.out.println(zoo);
					break;
				}
					
				default:{
					System.out.println("Comandes:");
					System.out.println("\tafegeix cocodril");
					System.out.println("\tafegeix vaca");
					System.out.println("\ttreure vaca");
					System.out.println("\ttreure cocodril");
					System.out.println("\tteure totes les vaques");
					System.out.println("\ttreure tots els cocodrils");
					System.out.println("\tmira");
					System.out.println("\tmostra animals");
					System.out.println("\tsurt");
					break;
				}				
			}	
		}
		sc.close();
	}
}
