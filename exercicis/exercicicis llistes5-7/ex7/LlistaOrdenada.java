package ex7;

import java.util.ArrayList;

import java.util.List;

public class LlistaOrdenada {
	
	private List<Integer> ol = new ArrayList<Integer>();
	
	LlistaOrdenada(){
		ol.add(1);
		ol.add(2);
		ol.add(3);
		ol.add(4);
		ol.add(5);
		ol.add(6);
	}
	
	public void addInt(int num){
	
		int pos = Math.abs(findInt(num));
		
		if (ol.get(pos) < num)
			ol.add(pos+1, num);
		else {
			ol.add(pos, num);
		}
		/*no funciona
		if (!ol.isEmpty()){
			int i = (ol.size())/2;
			int meitat = i;
			while (meitat >= 1){
				if (num < ol.get(i)){
					meitat /= 2;
					if(meitat == 0)
						i = 1;
					i -= meitat;
				}else if (num > ol.get(i)){
					meitat /= 2;
					if (meitat == 0) 
						i += 1;
					i += meitat;
				}
				else{
					meitat = 0;
				}
			}
			if (i > ol.size()-1)
				ol.add(num);
			else
				if(num > ol.get(i))
					ol.add(i+=1, num);
				else
					ol.add(i, num);
		}
		else
			ol.add(num);
	*/	
	}
	
	public void deleteInt(int num){
		
		int pos = findInt(num);

		if (pos >= 0 && ol.size() > 0)
			ol.remove(pos);
	}
	
	public int findInt(int num){
		
		int ini = 0;
		int fin = ol.size()-1;
		int centre = 0;
		
		
		while (ini <= fin){
			centre = (ini+fin)/2;	//calcular centre del segment
			if(ol.get(centre) == num){	//cas que troba el numero
				return centre;
			}else if ( num < ol.get(centre)){	//n�mero a buscar m�s petit
				fin = centre - 1;	//el final del segment el poso una posicio menys del que era el centre 
			}else{
				ini = centre + 1;	//el principi del segment el poso una posici� m�s del que era el centre.	
			}			
		}
		return centre - (centre * 2);//Si no el trobo retorno la posici� on hauria de ser en negatiu
		
	}
		
		
	
		/*no funciona
		int i = (ol.size()-1)/2;
		int meitat = i;
		while (meitat >= 1){
			if (num < ol.get(i)){
				meitat /= 2;
				if(meitat == 0)
					i -= 1;
				i -= meitat;
			}else if (num > ol.get(i)){
				meitat /= 2;
				if (meitat == 0) 
					i += 1;
				i += meitat;
			}
			else 
				return i;
			
		}
		if (ol.get(i) == num)
			return i;
		else
			return -1;
			*/
	
	
	@Override
	public String toString(){
		return ol.toString();
	}
	
	
	
}
