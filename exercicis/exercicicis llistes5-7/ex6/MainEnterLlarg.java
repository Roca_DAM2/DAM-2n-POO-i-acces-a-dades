package ex6;

public class MainEnterLlarg {

	public static void main(String[] args) {
		
		
		String str1 = "111111"; 
		String str2 = "9999";
		Byte[] bt = {1,2,3,4,7,9,0,8,6,5,9};
		long lng = (long) 1234566785;
		EnterLlarg num1 = new EnterLlarg(str1);
		EnterLlarg num2 = new EnterLlarg(str2);
		EnterLlarg num3 = new EnterLlarg(bt);
		EnterLlarg num4 = new EnterLlarg(lng);
		EnterLlarg.suma(num4, EnterLlarg.suma(num3, EnterLlarg.suma(num1, num2)));
	}

}
