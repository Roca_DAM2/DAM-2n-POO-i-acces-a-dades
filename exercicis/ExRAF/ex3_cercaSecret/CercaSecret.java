package cercaSecret;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class CercaSecret {
	
	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
	    for (int i=0; i<nChars; i++) {
	        ch=fitxer.readChar();
	        if (ch != '\0')
	            b.append(ch);
	    }
	    return b.toString();
	}
	
	private static long buscaBin(RandomAccessFile fitxer, int num)throws IOException{
		int ini = 0;
		int fin = 999;
		int centre = 0;
		int codi = 0;
		
		while ( ini <= fin){
			centre = (ini+fin)/2;
			fitxer.seek(centre*10);
			codi = fitxer.readInt();
			if(codi == num){
				return centre;
			}
			else if (num < codi)
				fin = centre - 1;
			else 
				ini = centre + 1;		
		}
		return centre - (centre * 2);
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int num = 0;
		
		try (RandomAccessFile fitxer = new RandomAccessFile("codis.bin", "r")) {
			
			System.out.println("Introdueix el N�mero per saber el codi: ");
			num = sc.nextInt();
			
			long pos = buscaBin(fitxer, num);
			
			if (pos < 0)
				System.out.println("El n�mero no te codi.");
			else{
				fitxer.seek(pos*10);
				fitxer.readInt();
				System.out.println("codi : "+readChars(fitxer, 3));
			}
		} catch (IOException e) {
			System.err.println(e);
		}
		
		sc.close();

	}

}
