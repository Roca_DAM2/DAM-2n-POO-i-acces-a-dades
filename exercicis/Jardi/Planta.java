import java.util.Random;

public abstract class Planta {
	
	protected boolean esViva = true ;
	protected int altura = 0 ;
	
	
	char getChar(int nivell){
		return ' ';
	}
	
	Planta creix(){
		if (esViva && altura < 10){
			altura++;
			if(altura == 10)
				esViva = false;
		}
		return null;
	}
	
	int escampaLlavor(){
		
		 int[] posLlavor = {-2,-1, 1, 2}; 
		 Random randomGenerator = new Random();

		return posLlavor[randomGenerator.nextInt(3)+1];
	}
	
	int getAltura(){
		return this.altura;
	}
	
	boolean esViva(){
		return this.esViva;
	}
	
	
}