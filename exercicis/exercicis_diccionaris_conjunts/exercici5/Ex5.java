package exercici5;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ex5 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Map<String, String> dic = new HashMap<String, String>();
		dic.put("Espanya", "Madrid");
		dic.put("Fran�a", "Par�s");
		dic.put("It�lia", "Roma");
		dic.put("Anglaterra", "Londres");
		dic.put("Alemanya", "Berl�n");
		
		System.out.println("Introdueix un pa�s: ");
		String pais = sc.nextLine();
		
		if (dic.containsKey(pais))
			System.out.println("Capital: "+dic.get(pais));
		else{
			String capital = "noCapital";
			while (!capital.equals("")){
				System.out.println("Introdueix la capital de "+pais+":");
				capital = sc.nextLine();
					if (!capital.equals(""))
						dic.put(pais, capital);
			}
		}
		
		System.out.println(dic.get(pais));
		sc.close();
	}

}
