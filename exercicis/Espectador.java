package zoo;

public class Espectador implements Esser{

	@Override
	public String accio(Zoo zoo) {
		
		Animal animal = zoo.mostraAnimal();
		String str = "";
		
		if (animal == null)
			str = "Un espectador no sap on mirar perqu� no troba animals";
		else{
			if(animal instanceof Cocodril)
				str = "Un espectador mira a un perill�s cocodril";
			else if (animal instanceof Vaca)
				str = "Un espectador mira una vaca";
		}
		return str;
	}

}
