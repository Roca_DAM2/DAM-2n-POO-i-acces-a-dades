package ex2Text;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GrepSimple {

	public static void main(String[] args) {
		
		String linea;
	
		if (args.length == 2){
			try (BufferedReader reader = new BufferedReader(new FileReader(args[1]))){
				while((linea = reader.readLine()) != null){
					if(linea.contains(args[0]))
						System.out.println(linea);
				}
			} catch (IOException e){
				System.err.println("No es troba l'arxiu.");
			}
		}else{
			System.err.println("�s: java GrepSimple <paraula> <fitxer>");
		}
	}
}
