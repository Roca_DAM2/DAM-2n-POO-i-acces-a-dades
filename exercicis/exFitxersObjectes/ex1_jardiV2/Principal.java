package jardiV2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * A la classe Principal creem un jard�, hi posem alguna planta,
 * i fem passar un torn cada cop que l'usuari introdueix una l�nia
 * de text.
 */
public class Principal {
	/**
	 * El jard� que es crea, de mida 40.
	 */
	private Jardi jardi;

	/**
	 * El main es limita a crear un objecte de tipus Principal. El
	 * constructor d'aquest objecte fa la resta. Aix� �s habitual, com
	 * que treballem amb un objecte, evita haver de declarar la resta de
	 * propietats i m�todes de la classe com static.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Principal();
	}

	/**
	 * S'inicialitza el jard�, i es demana entrada a l'usuari. Cada l�nia que
	 * s'introdueix avan�a un torn el jard�. Si l'usuari introdueix la cadena
	 * "surt" s'acaba el programa.
	 */
	public Principal() {
		Scanner sc = new Scanner(System.in);
		String ordre = "";
		
		try (ObjectInputStream load = new ObjectInputStream(new FileInputStream("jardi.sav"));){
			Object o = load.readObject();
			jardi = (Jardi) o;			//Si hi ha un fitxer amb un jard� guardat el recuperem
		} catch (IOException ex){		//En el cas de que no es trobi el fitxer o la classe el creem de nou
			jardi = new Jardi(40);
			jardi.posaElement(new Altibus(), 10);
			jardi.posaElement(new Declinus(), 30);
		} catch (ClassNotFoundException ex){
			jardi =  new Jardi(40);
			jardi.posaElement(new Altibus(), 10);
			jardi.posaElement(new Declinus(), 30);
		}
		
		
		while (!ordre.equals("surt")) {
			jardi.temps();
			System.out.println(jardi.toString());
			ordre = sc.nextLine();
		}
		
		try (ObjectOutputStream save = new ObjectOutputStream(new FileOutputStream("jardi.sav"));){
			save.writeObject(jardi);
		}catch(IOException ex){
			System.err.println(ex);
		}
		sc.close();
	}

}
