package fitxers1;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Deque;
import java.util.LinkedList;


public class ExFitxer1i2 {

	public static void main(String[] args) {
		
		Deque<Path> pila =new LinkedList<Path>();
		
		if (args.length == 1) {
            Path dir = Paths.get(args[0]);
			pila.push(dir);
			if (Files.isDirectory(dir)) {
				while(pila.size()>0){
					System.out.println("\nFitxers del directori " + pila.peek());     	
					try (DirectoryStream<Path> stream = Files.newDirectoryStream(pila.pop())) {
						String str;
		                    for (Path fitxer: stream) {
		                    	if(Files.isDirectory(fitxer)){
		                    		str= "Directori";
		                    		pila.push(fitxer);
			                    }else
			                    	str="Fitxer   ";
			                    	str +="\t";
			                    if(Files.isReadable(fitxer))
			                    	str += "R";
			                    else
			                    	str += " ";
			                    if(Files.isWritable(fitxer))
			                    	str += "W";
			                    else
			                    	str += " ";
			                    if(Files.isExecutable(fitxer))
			                    	str += "X";
			                    else
			                    	str += " ";
			                    
			                    str += "\t"+ fitxer.getFileName();
			                    System.out.println(str);
		                    }
            		} catch (IOException | DirectoryIteratorException ex) {         
            			System.err.println(ex);
            		}
				}
            } else {
            	System.err.println(dir.toString()+" no �s un directori");
            }
        } else {
            System.err.println("�s: java LlistarDirectori <directori>");
        }
    }
}


