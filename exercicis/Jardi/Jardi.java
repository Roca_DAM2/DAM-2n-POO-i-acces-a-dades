
public class Jardi {
	private int mida;
	private Planta jardi[];
	
	public Jardi(int m) {
		mida = m ;
		jardi = new Planta[mida];
	}
	
	public void temps(){
		
		
		Planta p;
		
		for (int i = 0; i < mida; i++ ){  		//Recorrer  Jardi
			
			if (jardi[i] instanceof Planta){	//si pos jardi no esta buida	
				if (jardi[i].esViva == true){	//si la planta esta viva
					p = jardi[i].creix();
					if(p != null){				// su retorna algo
						if(p instanceof Llavor) //si es una llavor la escampo y la planto on toqui
							plantaLlavor(p, p.escampaLlavor()+i);		
						else 
							jardi[i] = p;		//i es una planta la planto on era la llavor
					}
				}
				else 	//si es morta borro la planta
					jardi[i] = null; 
			}
		}
	}
	
	@Override
	public String toString(){
		
		StringBuilder resultat = new StringBuilder();
		for(int i = 12; i >= 0; i-- ){
			for(int j = 0; j < mida; j++){
				if(jardi[j] instanceof Planta){
					resultat.append( jardi[j].getChar(i));
				}
				else
					resultat.append(' '); 
			}
			
			resultat.append('\n') ;
		}
		for(int i = 0; i < mida; i++)
			resultat.append('_') ;
		
		return resultat.toString();
	}
	
	public boolean plantaLlavor(Planta novaPlanta, int pos){
		if ((pos >=0 && pos < mida) && jardi[pos] == null){
			jardi[pos] = novaPlanta;
			return true;
		}
		else
			return false;
	}
	

}
