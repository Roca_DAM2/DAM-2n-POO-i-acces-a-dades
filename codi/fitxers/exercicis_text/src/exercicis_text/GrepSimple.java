package exercicis_text;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GrepSimple {
	public static void main(String[] args) {
		if (args.length == 2) {
			Path path = Paths.get(args[1]);
			if (Files.isRegularFile(path) && Files.isReadable(path)) {
				grep(args[0], args[1]);
			} else {
				System.err.println("No es pot accedir a " + args[1]);
			}
		} else {
			System.err.println("Sintaxi: GrepSimple cadena fitxer");
		}

	}

	public static void grep(String cadena, String fileName) {
		String linia;
		try (BufferedReader lector = new BufferedReader(new FileReader(fileName))) {
			while ((linia = lector.readLine()) != null) {
				if (linia.contains(cadena))
					System.out.println(linia);
			}
		} catch (FileNotFoundException e) {
			System.err.println("No s'ha pogut accedir al fitxer: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Error en la lectura: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
