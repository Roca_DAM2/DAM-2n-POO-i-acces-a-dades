package exXML1;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LectorDOMObjectes {

	public static void llegeixMascotes( ArrayList<Persona> persones ) throws IOException {
		try {
			LectorDOMObjectes lector = new LectorDOMObjectes();
			
		
			try (FileInputStream reader = new FileInputStream("persones.xml")) {
				
				DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document document = documentBuilder.parse(reader);
	
				// document.getDocumentElement().getNodeName()) �s "persones"
				NodeList personesNodeList = document.getElementsByTagName("persona");
				
				for (int i=0; i<personesNodeList.getLength(); i++) { // NodeList no implementa Iterable
					Node personaNode = personesNodeList.item(i);
	
					if (personaNode.getNodeType() == Node.ELEMENT_NODE) {
						Element personaElement = (Element) personaNode;
						
						persones.add(new Persona(
								getNodeValue("nom", personaElement),
								Integer.parseInt(getNodeValue("edat", personaElement))
						));		
					}
				}	
			} catch (IOException | ParserConfigurationException | SAXException e) {
				throw new IOException(e);
			}
			lector.mostraPersones(persones);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private static String getNodeValue(String etiqueta, Element element) {
		NodeList nodeList = element.getElementsByTagName(etiqueta).item(0).getChildNodes();
		Node node = nodeList.item(0);
		return node.getNodeValue();
	}
	
	public void mostraPersones(ArrayList<Persona> persones) {
		for (Persona p : persones)
			System.out.println(p.getEdat()+" "+p.getNom());
	}
}


