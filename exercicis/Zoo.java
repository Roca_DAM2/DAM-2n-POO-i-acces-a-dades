package zoo;

import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class Zoo { 
	
	 private CopyOnWriteArrayList<Animal> animals = new CopyOnWriteArrayList<Animal>();
	 private Espectador espectador1 = new Espectador();
	 static final Random rand = new Random();
	
	public Animal mostraAnimal(){
		
		if (animals.isEmpty()){
			return null;
		}
		else
			 return animals.get(rand.nextInt(animals.size()));	
	}
	
	public void afegeixAnimal(String animal){
		
		if(animal.toLowerCase().equals("vaca")){
			animals.add(new Vaca());
		}
		else if(animal.toLowerCase().equals("cocodril")){
			animals.add(new Cocodril());
		}
	}
	
	public void suprimeixAnimal(String animal){
		
		boolean eliminat = false;
		int i = 0;
		
		if(animal.toLowerCase().equals("vaca")){
			while(!(eliminat) && i < animals.size()){
				if (animals.get(i) instanceof Vaca && !(eliminat)){
					animals.remove(i);
					eliminat = true;
				}
				i++;
			}
		}
		else if(animal.toLowerCase().equals("cocodril")){
			while(!(eliminat) && i < animals.size()){
				if (animals.get(i) instanceof Cocodril && !(eliminat)){
					animals.remove(i);
					eliminat = true;
				}
				i++;
			}
		}
	}
	
	
	public void suprimeixAnimal(Animal animal){
		animals.remove(animal);
	}
	
	public void suprimeixTots(String tipusAnimal){
		if (tipusAnimal.toLowerCase().equals("vaca")){
			for(Animal animal : animals){
				if (animal instanceof Vaca)
					animals.remove(animal);
			}
		}
		else if (tipusAnimal.toLowerCase().equals("cocodril")){
				for(Animal animal : animals){
					if (animal instanceof Cocodril)
						animals.remove(animal);
				}
		}
	}
	
	public void mira(){
		
		if(rand.nextInt(2) == 0)
			System.out.println(espectador1.accio(this));
		else{
			if(animals.isEmpty())
				System.out.println("Quin zoo m�s avorrit! No t� cap animal");
			else
				System.out.println(animals.get(rand.nextInt(animals.size())).accio(this));
		}		
	}
	
	@Override
	public String toString(){
		String str = "";
		
		for(Animal animal : animals){
			if (animal instanceof Vaca)
				str += "vaca ";
			else if (animal instanceof Cocodril)
				str +="Cocodril ";
		}
		return str;
	}
}



























