package exXML1;

import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class LectorSAX {
	
	public static void llegeixSAX(){
		try {
			XMLReader processadorXML = XMLReaderFactory.createXMLReader();
			processadorXML.setContentHandler(new ControladorSAX());
			processadorXML.parse(new InputSource("persones.xml"));
		} catch (SAXException | IOException e) {
			System.err.println(e.getMessage());
		}
	}
}