package detectorText;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DetectorText {

	public static void main(String[] args) {
		
		int b = 0;
		char caracter;
		if (args.length > 0) {
			for(int i=0; i<args.length; i++){
				Path arxiu = Paths.get(args[i]);
				if (!Files.isDirectory(arxiu)) {
					try  (DataInputStream reader = new DataInputStream(new FileInputStream(arxiu.toString()))){
						
						while ((b = reader.read()) != -1){
							caracter = (char) b;
							if(Character.isAlphabetic(caracter))
								System.out.print(caracter);
						}
			        } catch (IOException ex) {
			            System.err.println(ex);
			        }
				}else
					System.out.println("No es un arxiu.");
			}
		}else
			System.out.println("Falta l'arxiu a llegir.");
			
	}

}
