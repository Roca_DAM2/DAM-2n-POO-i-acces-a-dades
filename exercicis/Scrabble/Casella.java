package scrabble;

public class Casella implements Cloneable{
	private char lletra;
	private int modLletra;
	private int modPar;


	public Casella(char lletra, int modL, int modP ){
		
		if(Character.isLetter(lletra))
			this.lletra = Character.toUpperCase(lletra);
		else
			throw new IllegalArgumentException("Hi ha caracters que no son lletres");
		
		if(modL > 3 && modL < 1)
			modLletra = 1;
		else 
			modLletra = modL;
		
		if(modP > 3 && modP < 1)
			modPar = 1;
		else 
			modPar = modP;	
	}
	@Override
	public ScrabbleEx clone(){
		try{
			return (ScrabbleEx) super.clone();
		}
		catch (CloneNotSupportedException e){
			e.printStackTrace();
			return null;
		}
		
	}
	public char getLletra() {
		return lletra;
	}
	public void setLletra(char lletra) {
		this.lletra = lletra;
	}
	public int getModLletra() {
		return modLletra;
	}
	public int getModPar() {
		return modPar;
	}

	


	
}