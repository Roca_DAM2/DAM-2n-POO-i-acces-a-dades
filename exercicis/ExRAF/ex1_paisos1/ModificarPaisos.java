package paisos1;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class ModificarPaisos {
	 private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
	        StringBuilder b = new StringBuilder();
	        char ch = ' ';
	        for (int i=0; i<nChars; i++) {
	            ch=fitxer.readChar();
	            if (ch != '\0')
	                b.append(ch);
	        }
	        return b.toString();
	    }

	    public static void main(String[] args) {
	        String nom;
	        String codIso;
	        String capital;
	        int id, poblacio;
	        String opcio = "";
	        StringBuilder str = new StringBuilder();
	        long pos=0;
	        
	        Scanner scanner = new Scanner(System.in);
	        
	        
	        System.out.println("Introdueix n�mero de registre: ");
	        id=scanner.nextInt();
	        scanner.nextLine();
	        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
	            // per accedir a un registre multipliquem per la mida de
	            // cada registre.
	            pos=(id-1)*174;

	            if (pos<0 || pos>=fitxer.length())
	                throw new IOException("N�mero de registre inv�lid.");
	            
	            //nostrem la informacio del registre triat
	            fitxer.seek(pos);
	            fitxer.readInt(); // Saltem l'id
	            nom = readChars(fitxer, 40);
	            codIso = readChars(fitxer, 3); 
	            capital = readChars(fitxer, 40);
	            poblacio = fitxer.readInt();
	            System.out.println("Pa�s: "+nom+" ("+codIso+"), capital: "+capital+", poblaci� actual: "+poblacio);
	            
	            
	            while(!opcio.equals("0")){	
	            	System.out.println("\t1.-Canviar nom.");
	            	System.out.println("\t2.-Canviar codi.");
	            	System.out.println("\t3.-Canviar capital.");
	            	System.out.println("\t4.-Canviar poblaci�.");
	            	System.out.println("\t0.-Sortir.");
	            	System.out.println("Tria una opci�: ");
	            	opcio = scanner.next();
	            	
	            	fitxer.seek(pos);	//tornem a la posici� inicial del registre
	            	fitxer.readInt(); // Saltem l'id
	            	str = new StringBuilder();
	            	
	            	switch (opcio){
	            	case "1":
	            		System.out.println("Introdueix el nou nom: ");
	            		str.append(scanner.next());
	            		str.setLength(40);
	     	            fitxer.writeChars(str.toString());
	     	            break;
	            	case "2":
	            		
	            		System.out.println("Introdueix el nou codi ISO: ");
	            		str.append(scanner.next());
	            		str.setLength(3);
	            		System.out.println(str.toString());
	     	            readChars(fitxer,40);
	     	            fitxer.writeChars(str.toString());
	     	            break;
	            	case "3": 
	            		System.out.println("Introdueix la nova capital: ");
	            		str.append(scanner.next());
	            		str.setLength(40);
	     	            readChars(fitxer,43); //Saltem Nom i ISO
	     	            fitxer.writeChars(str.toString());
	     	            break;
	            	case "4":
	            		readChars(fitxer,83); //Saltem Nom, ISO i capital
	            		System.out.println("Introdueix la nova poblaci�: ");
	     	            poblacio = scanner.nextInt();
	     	            scanner.nextLine();
	     	            if (poblacio >= 0) {
	     	                fitxer.writeInt(poblacio);
	     	            } else {
	     	                System.err.println("La poblaci� ha de ser positiva.");
	     	            }
	     	            break;
	            	case "0":
	            		System.out.println("Adeu");
	            		break;
	            	default:
	            		System.out.println("Opci� incorrecta.");
	            		break;
	            	}
	            		
	            }
	            
	        } catch (IOException e) {
	            System.err.println(e.getMessage());
	        }
	        scanner.close();
	    }
}


