package ex6;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class EnterLlarg {

	private List<Integer> enterLlarg = new ArrayList<Integer>();

	public EnterLlarg(Long num) {

		this(num.toString());

	}

	public EnterLlarg(Byte[] num) {

		for (Byte b : num) {
			enterLlarg.add(Integer.parseInt(b.toString()));
		}

		System.out.println(this.toString());
	}

	public EnterLlarg(String num) {

		for (char c : num.toCharArray()) {
			if (Character.isDigit(c))
				enterLlarg.add(Character.getNumericValue(c));
		}

		System.out.println(this.toString());
	}

	@Override
	public String toString() {

		String numLlarg = "";

		for (Integer digit : this.enterLlarg)
			numLlarg += digit.toString();

		return numLlarg;
	}

	public static EnterLlarg suma(EnterLlarg num1, EnterLlarg num2) {

		ListIterator<Integer> it1 = num1.enterLlarg.listIterator(num1.enterLlarg.size());
		ListIterator<Integer> it2 = num2.enterLlarg.listIterator(num2.enterLlarg.size());
		StringBuilder resultat = new StringBuilder();
		Integer resta = 0;
		Integer suma = 0;

		while (it1.hasPrevious() && it2.hasPrevious()) {
			suma = it1.previous() + it2.previous() + resta;
			if (suma > 9) {
				resultat.insert(0, suma -= 10);
				resta = 1;
			} else {
				resultat.insert(0, suma);
				resta = 0;
			}
		}
		
		if (it1.hasPrevious()) {
			while (it1.hasPrevious()){
				suma = it1.previous() + resta;
				if (suma > 9) {
					resultat.insert(0, suma -= 10);
					resta = 1;
				} else {
					resultat.insert(0, suma);
					resta = 0;
				}
			}
		}
		if (it2.hasPrevious()) {
			while (it2.hasPrevious()) {
				suma = it2.previous() + resta;
				if (suma > 9) {
					resultat.insert(0, suma -= 10);
					resta = 1;
				} else {
					resultat.insert(0, suma);
					resta = 0;
				}
			}
		}

		if (resta > 0)
			resultat.insert(0, resta);

		return  new EnterLlarg(resultat.toString());
	}
}
