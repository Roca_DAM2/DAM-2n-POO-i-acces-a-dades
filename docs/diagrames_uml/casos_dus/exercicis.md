## Casos d'ús

### Exercicis

1. Redacta el cas d'ús corresponent a la compra d'un bitllet de tren a un
terminal de rodalies. Fes el diagrama corresponent.

2. Redacta el cas d'ús corresponent al prèstec d'un llibre en una biblioteca
pública. Fes el diagrama corresponent.
