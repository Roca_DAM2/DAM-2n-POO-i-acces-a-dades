package scrabble;

public class ScrabbleEx implements Comparable<ScrabbleEx>{
	
	private Casella[] vectorCa;
	
	private static final String[] lletres = {	"AEILNORSTU",
												"DG",
												"BCMP",
												"FHVWY",
												"K",
												"JX",
												"QZ"};




	public ScrabbleEx(Casella vecCas[]){
		vectorCa = vecCas;
	}

public int valor(){
		
		char c; 
		int valor = 0;
		int modL;

		for(int i = 0; i < vectorCa.length; i++){
			boolean trobada = false;
			c = vectorCa[i].getLletra();
			modL = vectorCa[i].getModLletra();
			for(int j = 0; j < lletres.length; j++){
				if(trobada == false){
					for (int k = 0; k < lletres[j].length();k++){
						if(lletres[j].charAt(k) == c){
							if(j < 5)
								valor += (j+1)*modL;
							else if(j == 5)
								valor += 8*modL;
							else 
								valor += 10*modL;
							
							trobada = true;
						}	
					}
				}
			}	
		}
		for(int i = 0; i < vectorCa.length; i++){
			valor *= vectorCa[i].getModPar();
		}
		return valor;
	}

	@Override
	public int compareTo(ScrabbleEx arg0) {
		
		return this.valor()-arg0.valor();
	}
}