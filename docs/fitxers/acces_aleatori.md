## Fitxers d'accés aleatori

Un fitxer d'accés aleatori és un fitxer binari que guarda una sèrie de
registres de la mateixa mida en bytes.

Com que es pot calcular a priori la posició que ha d'ocupar un registre si
en sabem el seu índex, és senzill recuperar qualsevol dels registres
sense haver de llegir la resta.

Això permet tenir un gran nombre de registres al mateix fitxer i que no
sigui necessari guardar-lo tot a memòria o recórre'l tot per cercar un
registre que necessitem.

En Java, la classe *RandomAccessFile* representa un fitxer d'aquest tipus.

Els següents exemples treballen sobre la següent classe, que guarda les
dades bàsiques d'un país:

```java
public class Pais {
	private String nom;
	private String codiISO;
	private int poblacio;
	private String capital;

	public Pais(String nom, String codiISO, String capital) {
		if (codiISO.length() != 3)
			throw new IllegalArgumentException("Els codis ISO sempre tenen 3 caràcters.");
		this.nom = nom;
		this.codiISO = codiISO;
		this.capital = capital;
	}
	public int getPoblacio() {
		return poblacio;
	}
	public void setPoblacio(int poblacio) {
		if (poblacio < 0)
			throw new IllegalArgumentException("La població ha de ser positiva.");
		this.poblacio = poblacio;
	}
	public String getNom() {
		return nom;
	}
	public String getCodiISO() {
		return codiISO;
	}
	public String getCapital() {
		return capital;
	}
  @Override
	public String toString() {
		return "País "+nom+" ("+codiISO+"), capital: "+capital+", població: "+poblacio;
	}
}
```

### Escriptura d'un fitxer d'accés aleatori

El següent programa guarda algunes dades de països de l'est d'Europa en un
fitxer d'accés aleatori:

```java
public class EscriptorRandomAccessFile {

	public static void main(String[] args) {
		Pais[] paisos = new Pais[5];

		paisos[0] = new Pais("Albània", "ALB", "Tirana");
		paisos[1] = new Pais("Bòsnia i Hercegovina", "BIH", "Sarajevo");
		paisos[2] = new Pais("Croàcia", "HRV", "Zagreb");
		paisos[3] = new Pais("Montenegro", "MNE", "Podgorica");
		paisos[4] = new Pais("Sèrbia", "SRB", "Belgrad");
		paisos[0].setPoblacio(3582205);
		paisos[1].setPoblacio(4498976);
		paisos[2].setPoblacio(4800000);
		paisos[3].setPoblacio(630548);
		paisos[4].setPoblacio(8196411);

		StringBuilder b = null;
		try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
			for (int i=0; i<paisos.length; i++) {
				b = new StringBuilder(paisos[i].getNom());
				b.setLength(40); //Asigna mida de 40 caracters al contingut de StringBuilder
				fitxer.writeInt(i+1);						//id  ------> int  (4 bytes)
				fitxer.writeChars(b.toString());			//nom ------> char (2 bytes) * 40 caràcters
				fitxer.writeChars(paisos[i].getCodiISO());	//Codi ISO -> char (2 bytes) * 3 caràcters
				b = new StringBuilder(paisos[i].getCapital());
				b.setLength(40);
				fitxer.writeChars(b.toString());			//Capital --> char (2 bytes) * 40 caràcters
				fitxer.writeInt(paisos[i].getPoblacio());	//població -> int  (4 bytes)
												//total per país: 174 bytes
			} // Total: 174 bytes * 5 països = 870 bytes
		} catch (IOException e) {
			System.err.println(e);
		}
	}
}
```

És important assegurar que tots els registres ocupen exactament el mateix. Amb
dades simples com *int* o *double* això és automàtic, però en el cas de
cadenes de text ho hem de forçar. En aquest exemple s'ha utilitzat un
*StringBuilder* per crear una cadena d'exactament 40 caràcters, independentment
de la mida de la cadena a guardar-hi.

Després de l'execució del programa podem comprovar que, efectivament, el nostre
fitxer ocupa exactament 870 bytes. Si l'obrim amb un editor hexadecimal podrem
rastrejar fàcilment les dades que hi hem guardat.

Dels diversos modes d'obertura que hi ha per un fitxer d'accés aleatori els més
importants són:

- *r*: obertura només per lectura
- *rw*: obertura per lectura i escriptura. El fitxer es crea si no existia.

### Lectura d'un fitxer d'accés aleatori

Al següent exemple recuperem les dades que hem guardat al fitxer anterior i
les mostrem per pantalla:

```java
public class LectorRandomAccessFile {

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i=0; i<nChars; i++) {
			ch=fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}

	public static void main(String[] args) {
		Pais p;
		String nom, capital, codiISO;
		int poblacio;
		try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "r")) {
			while (fitxer.getFilePointer() != fitxer.length()) {
				System.out.println("País: "+fitxer.readInt());
				nom = readChars(fitxer, 40);
				codiISO = readChars(fitxer, 3);
				capital = readChars(fitxer, 40);
				poblacio = fitxer.readInt();
				p = new Pais(nom, codiISO, capital);
				p.setPoblacio(poblacio);
				System.out.println(p);
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}
}
```

Cal notar que *RandomAccessFile* no té un mètode *getChars()* que permeti
recuperar una colla de caràcters d'un sol cop. Per això hem implementat el
mètode auxiliar per llegir cadenes.

Aquest mètode és delicat: cal guardar fins que arribem al màxim de caràcters
que pot tenir una cadena, o fins que ens topem amb un caràcter 0, però encara
que no els guardem tots, hem de llegir fins al màxim de caràcters. Si no,
deixaríem el punter de lectura del fitxer a mitges, i les següents lectures
serien errònies.

Per tal de recuperar totes les dades cal que recordem exactament en quin
ordre s'han guardat, i quants caràcters màxims tenien les cadenes.

La sortida del programa és:

```
País: 1
País Albània (ALB), capital: Tirana, població: 3582205
País: 2
País Bòsnia i Hercegovina (BIH), capital: Sarajevo, població: 4498976
País: 3
País Croàcia (HRV), capital: Zagreb, població: 4800000
País: 4
País Montenegro (MNE), capital: Podgorica, població: 630548
País: 5
País Sèrbia (SRB), capital: Belgrad, població: 8196411
```

Fins aquí no hem fet res que no haguéssim pogut fer amb un *DataInputStream* i
un *DataOutputStream*. La gràcia dels fitxers d'accés aleatori, però, és que
podem accedir a un dels registres directament, tant si el volem llegir com
si el volem actualitzar. També podem afegir registres nous al final del fitxer
sense have de llegir els registres existents abans.

### Actualització d'un registre

El següent exemple pregunta un número de registre i permet actualitzar la
població del país corresponent:

```java
public class LectorRegistre {
	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i=0; i<nChars; i++) {
			ch=fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}

	public static void main(String[] args) {
		String nom;
		int id, poblacio;
		long pos=0;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Introdueix número de registre: ");
		id=scanner.nextInt();
		scanner.nextLine();
		try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
			// per accedir a un registre multipliquem per la mida de
			// cada registre.
			pos=(id-1)*174;

			if (pos<0 || pos>=fitxer.length())
				throw new IOException("Número de registre invàlid.");

			fitxer.seek(pos);
			fitxer.readInt(); // Saltem l'id
			nom = readChars(fitxer, 40);
			readChars(fitxer, 43); // Ens saltem el codi ISO i la capital
			poblacio = fitxer.readInt();
			System.out.println("País: "+nom+", població actual: "+poblacio);
			System.out.println("Introdueix la nova població: ");
			poblacio = scanner.nextInt();
			scanner.nextLine();
			if (poblacio >= 0) {
				pos = fitxer.getFilePointer()-4; // tornem enrere per sobreescriure la població
				fitxer.seek(pos);
				fitxer.writeInt(poblacio);
			} else {
				System.err.println("La població ha de ser positiva.");
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		scanner.close();
	}
}
```

Hi ha algunes coses importants a destacar en aquest exemple:

- La variable *pos* s'utilitza per situar la posició des d'on es llegirà o
cap on s'escriurà en el fitxer. És de tipus *long* perquè aquest és el
tipus que espera *seek()* i que retorna *getFilePointer()*.

- El mètode *seek()* permet posar el punter a qualsevol posició del fitxer.
Cal conèixer molt bé la seva estructura per calcular correctament els bytes
que ens hem de saltar des del principi.

- El mètode *getFilePointer()* retorna la posició on hi ha el punter en un
moment donat.

Tots aquests exemples es podrien unir en un sol programa i podríem encapsular
les operacions d'entrada i sortida per simplificar-ne l'ús. Per exemple,
podríem tenir un mètode que llegís un registre determinat i un mètode que
escrivís un registre, i aprofitar-los per fer totes les actualitzacions
necessàries.

Una altra millora seria no haver de guardar un identificador i permetre cercar
els països directament pel seu nom. Això implicaria guardar sempre els països
ordenats pel seu nom, i implementar algorismes optimitzats (cerca binària)
per trobar ràpidament un registre a partir del seu nom, sense haver de llegir
tot el fitxer.
