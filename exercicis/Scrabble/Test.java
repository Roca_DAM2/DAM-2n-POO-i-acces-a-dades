package scrabble;

import java.util.Arrays;

public class Test {

	public static void main(String[] args) {
		
		
		//primera part
		
		Scrabble[] a =	{	new Scrabble("Xavi"),
							new Scrabble("Sabadell"),
							new Scrabble("Pajarito"),
							new Scrabble("objecte"),
							new Scrabble("muntanya"),
							new Scrabble("pantalla"),
							new Scrabble("final")
		};
		
		//segona part
		
		Arrays.sort(a);
		for(int i = 0; i < a.length;i++){
			System.out.println(a[i]);
		}
		
		String[] b =	{	new String("Xavi"),
							new String("Sabadell"),
							new String("Pajarito"),
							new String("objecte"),
							new String("muntanya"),
							new String("pantalla"),
							new String("final")
		};
		
		Arrays.sort(b, ComparadorScrabble.getInstance());
		for(int i = 0; i < b.length;i++){
			System.out.println(b[i]);
		}
	
		
		//part 3
		
		Casella[] paraula =	{
				new Casella('X', 1, 1),
				new Casella('A', 1, 1),
				new Casella('V', 1, 2),
				new Casella('I', 1, 1)
		};
		
		ScrabbleEx c = new ScrabbleEx(paraula);
		
		System.out.println(c.valor());
		
	}

}
