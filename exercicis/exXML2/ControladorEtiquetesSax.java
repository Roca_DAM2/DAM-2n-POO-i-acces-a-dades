package exXML2;

import java.util.HashSet;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ControladorEtiquetesSax extends DefaultHandler{
	
	
	Set<String> tags = new HashSet();

	// M�tode de resposta a un esdeveniment de tipus 'inici d'element'
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		tags.add(localName);
		
			
		
	}
	
	public String retornaElements(){
		String elements = "";
		for (String s : tags){
			elements += s+" ";
		}
		elements += "Total: "+tags.size();
		return elements;
	}

	

}
