package paisos2;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class SobreescriurePais {

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
        StringBuilder b = new StringBuilder();
        char ch = ' ';
        for (int i=0; i<nChars; i++) {
            ch=fitxer.readChar();
            if (ch != '\0')
                b.append(ch);
        }
        return b.toString();
    }

    public static void main(String[] args) {
        String nom;
        String codIso;
        String capital;
        int id, poblacio;
        StringBuilder str = new StringBuilder();
        long pos=0;
        
        Scanner scanner = new Scanner(System.in);
        
        
        System.out.println("Introdueix n�mero de registre: ");
        id=scanner.nextInt();
        scanner.nextLine();
        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
            // per accedir a un registre multipliquem per la mida de
            // cada registre.
            pos=(id-1)*174;

            if (pos<0 || pos>=fitxer.length())
                throw new IOException("N�mero de registre inv�lid.");
            
            //nostrem la informaci� del registre que sobreescriurem
            fitxer.seek(pos);
            fitxer.readInt(); // Saltem l'id
            nom = readChars(fitxer, 40);
            codIso = readChars(fitxer, 3); 
            capital = readChars(fitxer, 40);
            poblacio = fitxer.readInt();
            System.out.println("Pa�s: "+nom+" ("+codIso+"), capital: "+capital+", poblaci� actual: "+poblacio);
            
            
        	fitxer.seek(pos);	//tornem a la posici� inicial del registre
        	fitxer.readInt(); // Saltem l'id
        	
        	
    		str = new StringBuilder();
    		System.out.println("Introdueix el nou Pais: ");
    		str.append(scanner.next());
    		str.setLength(40); 
            fitxer.writeChars(str.toString());
       
            str = new StringBuilder();
    		System.out.println("Introdueix el nou codi ISO: ");
    		str.append(scanner.next());
    		str.setLength(3);
            fitxer.writeChars(str.toString());
            
            str = new StringBuilder();
    		str.setLength(40);
    		System.out.println("Introdueix la nova capital: ");
    		str.append(scanner.next());
    		str.setLength(40);    
            fitxer.writeChars(str.toString());
       
            str = new StringBuilder();
    		System.out.println("Introdueix la nova poblaci�: ");
            poblacio = scanner.nextInt();
            scanner.nextLine();
            if (poblacio >= 0) {
                fitxer.writeInt(poblacio);
            } else {
                System.err.println("La poblaci� ha de ser positiva.");
            }
        
        
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        scanner.close();
    }
}



