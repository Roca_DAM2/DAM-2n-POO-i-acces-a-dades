package scrabble;

import java.util.Comparator;

public class ComparadorScrabble implements Comparator<String> {
	
	private static final String[] lletres = {	"AEILNORSTU",
												"DG",
												"BCMP",
												"FHVWY",
												"K",
												"JX",
												"QZ"};

	private static ComparadorScrabble COMPARADOR_SCRABBLE = null;
	
	private ComparadorScrabble(){
		
	}
	
	private int valor(String s){
		
		char c; 
		int valor = 0;
		
		
		for(int i = 0; i < s.length(); i++){
			boolean trobada = false;
			c = s.charAt(i);
			for(int j = 0; j < lletres.length; j++){
				if(trobada == false){
					for (int k = 0; k < lletres[j].length();k++){
						if(lletres[j].charAt(k) == c){
							if(j < 5)
								valor += j+1;
							else if(j == 5)
								valor += 8;
							else 
								valor += 10;
							
							trobada = true;
						}	
					}
				}
			}
		}
		return valor;
	}
	
	@Override
	public int compare(String s1, String s2) {
		
		return valor(s1.toUpperCase()) - valor(s2.toUpperCase());
	}
	
	
	public static ComparadorScrabble getInstance(){
		if(COMPARADOR_SCRABBLE == null)
			COMPARADOR_SCRABBLE = new ComparadorScrabble();
		return COMPARADOR_SCRABBLE;
	}

}
