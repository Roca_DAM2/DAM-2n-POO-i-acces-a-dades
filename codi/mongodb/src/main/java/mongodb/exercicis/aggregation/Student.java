package mongodb.exercicis.aggregation;

public class Student {
	public final String name;
	public final int id;
	public double scoreQuiz;
	public double scoreHomework;
	public double scoreExam;
	
	public Student(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public double getFinalScore() {
		double finalScore = 0.1*scoreQuiz + 0.3*scoreHomework + 0.6*scoreExam;
		if (scoreExam<40 && finalScore>40)
			finalScore=40;
		return finalScore;
	}
	
	public String toString() {
		return "Alumne: "+name+" ("+id+") - Nota final: "+getFinalScore();
	}
}
