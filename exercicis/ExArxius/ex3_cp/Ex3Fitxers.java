package cp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Ex3Fitxers {

	public static void main(String[] args) {
		if (args.length == 2) {
			
			Path dir = Paths.get(args[0]);
			
			
			if(Files.isDirectory(dir)){ //afegir el nom de l'arxiu si posen el nom d'un directori
				char[] fitxer = args[1].toCharArray();
				int i = fitxer.length-1;
				StringBuilder nom = new StringBuilder();
				nom.setLength(0);
				do{
					nom.insert(0, fitxer[i]);
					i--;
				}while(fitxer[i+1] != '\\');
				
				dir = Paths.get(args[0]+nom);
				System.out.println(dir);
			}
			Path arxiu = Paths.get(args[1]);

			try {
				if(!Files.exists(dir))
					Files.createDirectories(dir);
				Files.copy(arxiu, dir, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("El arxiu " + arxiu.toString() + " no es pot copiar a " + dir.toString());
			}

		} else {
			System.err.println("�s: java cp <directori a copiar arxiu> <ruta arxiu> ");
		}
	}

}
