## La pizzeria d'en Luigi

En aquest exercici utilitzarem un ***Decorator Pattern*** per crear les
diverses pizzes que se serviran a la pizzeria d'en Luigi.

**Exemple d'execució:**

```
Benvingut a la pizzeria Luigi!

Aquestes són les nostres pizzes:  
marinera  
margarita 

Quina pizza voleu?
hawaiana
No hi ha cap pizza amb aquest nom
 
Voleu fer una altra comanda (n per sortir)?
s

Aquestes són les nostres pizzes:
marinera
margarita

Quina pizza voleu?
margarita
El preu de la comanda és de 10.25
Els ingredients de la pizza són: una base de pizza fina, tomàquet, formatge ratllat
Voleu continuar (s per acceptar)?
s
El cuiner estén una finíssima base de pizza sobre el marbre.
El cuiner utilitza una espàtula per escampar tomàquet per sobre la base.
El cuiner escampa formatge ratllat per sobre.
Aquí té la seva pizza!

Voleu fer una altra comanda (n per sortir)?
n
Fins la propera!
```

Es crearan les següents classes/interfícies amb les següents característiques:

### *Ingredient*

*Ingredient* serà la base de tots els ingredients per a les pizzes, tant les
bases com el que hi posem a sobre. A més, una combinació d'ingredients també
es podrà veure com un ingredient.

Contindrà els següents mètodes, tots sense codi:

* `recepta`: retorna la descripció per a cada ingredient concret com el
cuiner l'utilitza per fer la pizza.

* `getPreu`: retornarà el preu de l'ingredient o del grup d'ingredients.

* `descripcio`: retornarà el nom de l'ingredient, o la llista d'ingredients
separats per comes.

* `clone`: voldrem que tots els ingredients es puguin clonar, així que
imposarem que tinguin aquest mètode.

### *IngredientExtra*

*IngredientExtra* representarà només aquells ingredients que es posen sobre de
la pizza i combinacions d'aquests ingredients, és a dir, qualsevol ingredient
excepte les bases de les pizzes.

Contindrà els següents mètodes i atributs:

* `ingredient`: aquest atribut serà qualsevol altre ingredient. S'utilitzarà
per fer els compostos (per exemple, si a un ingredient formatge li posem aquí
un ingredient tomàquet, i a aquest una base, ja tindrem un compost al qual
més endavant anomenarem pizza Margarita):

![Diagrama objectes pizzeria](docs/estructures_emmagatzematge/imatges/pizzeria_objectes.png)

* Constructor: el constructor rebrà un ingredient i el guardarà a l'atribut
anterior.

* `recepta`, `descripcio` i `getPreu`: aquests tres mètodes es limitaran a
cridar el mètode corresponent de l'ingredient intern.

* `clone`: tots els ingredients s'han de poder clonar.

### *BaseFina*

*BaseFina* serà una base per a pizza. En aquest exercici serà l'única base
que farem, i totes les pizzes la tindran com a ingredient principal, on
s'afegiran els ingredients extres.

El preu d'una base serà de 6,25€.

Inicialitza la recepta i la descripció segons l'exemple. Tots els ingredients
s'han de poder clonar.

### *Formatge*, *Tomaquet* i *Tonyina*

*Formatge*, *Tomaquet* i *Tonyina* seran els ingredients que afegirem a les
pizzes.

Els mètodes *recepta*, *descripcio* i *getPreu* afegiran les seves pròpies
dades a les que ja es posaven a *IngredientExtra*. Tots els ingredients han
de ser clonables.

### *PizzaFactory*

Aquesta classe serà l'encarregada d'emmagatzemar les receptes per a les
pizzes i crear-les quan se li demanin.

Per fer-ho disposarà d'un atribut de tipus *Map* que associarà el nom de cada
pizza a una pizza ja feta d'aquell tipus (recorda que una pizza no és res més
que un ingredient).

* El mètode `addPizza` permetrà afegir noves pizzes a les disponibles.

* El mètode `getNomsPizzes` retornarà un conjunt immutable amb el nom de totes
les pizzes registrades.

* El mètode `getPizzaByName` rebrà el nom d'una pizza i retornarà una pizza
d'aquell tipus. Per fer-ho, agafarà la pizza que té emmagatzemada, la clonarà
i retornarà la còpia obtinguda. Cal llençar excepció si el nom de la pizza
no està registrat.

### *Restaurant*

*Restaurant* serà la classe on hi ha haurà el *main*.

El *main* registrarà primer les pizzes a un *PizzaFactory*: en aquest exemple
tindrem la pizza Margarita, que contindrà tomàquet i formatge, i la pizza
Marinera que contindrà tonyina i formatge.

Per crear una pizza, primer de tot crearem la massa. Després, passarem
aquesta massa com a ingredient compost al constructor del primer dels
ingredients, i aquest primer ingredient el passarem al constructor del segon
ingredient. Si hi haguessin més ingredients procediríem de la mateixa manera.

A continuació es passarà a demanar pizzes a l'usuari, com a l'exemple.