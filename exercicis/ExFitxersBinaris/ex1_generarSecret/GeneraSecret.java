package generarSecret;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class GeneraSecret {

	public static void main(String[] args) {
		
		Random rd = new Random();
		int num = 0;
		String codi;
		
		
		try (DataOutputStream wr = new DataOutputStream(new FileOutputStream("codis.bin"))){
			for (int i = 0; i < 1000; i++){
				num += (rd.nextInt(3)+1);
				codi = (String.valueOf(Character.toChars(rd.nextInt(25)+97)) + String.valueOf(Character.toChars(rd.nextInt(25)+97)) + String.valueOf(Character.toChars(rd.nextInt(25)+97)));
				wr.writeInt(num);
				wr.writeChars(codi);
				
				System.out.println(num+" "+codi);
			}
		
		 } catch (FileNotFoundException ex) {
	            System.err.println(ex);
	     } catch (IOException ex) {
	            System.err.println(ex);
	     }

	}

}
