package ex3Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GrepChungo {
	
	public static void main(String[] args){
		
		
		List<File> fitxers = new ArrayList<File>();
		File f;
		
		if (args.length >= 2) {  // Si els arguments son igual o major a 2
			String paraula = args[0];	//agafem la paraula que hi ha al primer argument
			if (args.length == 2) {		//Si els arguments son 2
				f = new File(args[1]);
				if(f.exists() && f.isDirectory()){ //cas que sigui un directori
					File[] llistaArxius = f.listFiles();	//agafem els arxius d'aquest
					for(File file : llistaArxius){	//per cada arxiu 
						if(file.isFile()){	// si es un arxiu
							fitxers.add(file); // l'afegim a un arraylist de fitxers
						}					
					}
					buscaParaules(paraula, fitxers);		//enviem la paraula i el array de fitxers a la funcio que buscara la paraula
				}else if(f.exists() && f.isFile()){		// si el segon argument es un fitxer
					fitxers.add(f);						//l'afegim al array
					buscaParaules(paraula, fitxers);		//enviem la paraula i el array de fitxers a la funcio que buscara la paraula
				}else{ // si no existeix
					System.err.println("�s: java GrepChungo <paraula> <directori>");
					System.err.println("�s: java GrepChungo <paraula> <fitxer>...");					
				}
				
			}else{	//cas de mes de 2 arguments
				for ( int i=1; i<args.length; i++){ // per cada argument que passem a part de la paraula
					f = new File(args[i]);	
					if(f.exists() && f.isFile()){ // si existeix i es un arxiu
						fitxers.add(f);		//l'afegim al array de fitxers 
					}
				}
				buscaParaules(paraula, fitxers); //enviem la paraula i el array de fitxers a la funcio que buscara la paraula
			}
		}else{
			System.err.println("�s: java GrepChungo <paraula> <directori>");
			System.err.println("�s: java GrepChungo <paraula> <fitxer>...");
		}
	}
	
	private static void buscaParaules(String paraula, List<File>fitxers){
		String linea;
		int num;
		for(File f : fitxers){
			System.out.println("Fitxer: "+ f.getName());
			num = 1;
			try (BufferedReader reader = new BufferedReader(new FileReader(f))){
				while((linea = reader.readLine()) != null){
					if(linea.contains(paraula)){
						System.out.println("Linea "+num+":" +linea); 
					}
					num++;
				}
			} catch (IOException e){
				System.err.println("No es troba l'arxiu.");
			}
		}
	}
}
