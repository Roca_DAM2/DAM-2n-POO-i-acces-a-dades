package components_simples;

import components_abstractes.ComponentSimple;

public class Motor extends ComponentSimple {

	public Motor(int potencia, int pes) {
		super(pes);
		setNom("Motor");
		setPotencia(potencia);
	}
	
	@Override
	public String toString() {
		return getNom() + "; potència: "+getPotencia()+", pes: "+getPes();
	}
}
