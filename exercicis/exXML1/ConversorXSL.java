package exXML1;

import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class ConversorXSL {
	
	public static void pasaXSL(){
		
		try (FileOutputStream escriptor = new FileOutputStream("persones.html")) {
			Source dades = new StreamSource("persones.xml");	//Fitxer origen
			Source estils = new StreamSource("persones_plantilla.xsl"); //Fitxer processador XSL
			
			Result resultat = new StreamResult(escriptor); //Fitxer dest�
			
			Transformer transformer = TransformerFactory.newInstance().newTransformer(estils);
			transformer.transform(dades, resultat);
		} catch (IOException | TransformerException e) {
			System.err.println(e.getMessage());
		}
		
	}

}
