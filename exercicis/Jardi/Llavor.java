
public class Llavor extends Planta{
	
	private Planta planta;
	private int temps = 0;
	
	public Llavor(Planta p){
		if(p instanceof Llavor)
			throw new IllegalArgumentException();
		else
			this.planta = p;
	}
	
	@Override
	public Planta creix(){
		if (esViva){
			this.temps++;
			if(temps == 5){
				return planta;	
			}	
		}
		return null;
	}
	
	@Override
	public char getChar(int nivell){
		if(nivell == 0)
			return '.';
		else
			return super.getChar(nivell);
		
	}
}
