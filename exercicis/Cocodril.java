package zoo;

public class Cocodril extends Animal{

	@Override
	public String mou(Zoo zoo) {
		return "Un cocodril neda estany amunt estany avall";
	}

	@Override
	public String alimenta(Zoo zoo) {
		
		String str = "";
		Animal animal = zoo.mostraAnimal();
		
		if (animal == null || animal == this)
			str = "Un cocodril busca a qui es pot menjar";
		else{
			if(animal instanceof Vaca){
				str = "Un cocodril es menja a una vaca!";
				zoo.suprimeixAnimal(animal);
			}
			else if (animal instanceof Cocodril){
				str = "Un cocodril es menja a un altre cocodril!";
				zoo.suprimeixAnimal(animal);	
			}
		}
		return str;
	}

	@Override
	public String expressa(Zoo zoo) {		
		return "Un cocodril obre una boca plena de dents";
	}
	
}
