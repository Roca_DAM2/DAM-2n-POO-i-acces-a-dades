package exercici3;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		
		
		//a) Crea un diccionari amb aquestes dades.
		
		Map<String, Integer> dic = new HashMap<String, Integer>();
		
		dic.put("tom�quets", 6);
		dic.put("flams", 4);
		dic.put("pizzes", 2);
		dic.put("llaunes de tonyina", 2);
		dic.put("blat de moro", 5);
		dic.put("enciam", 1);
		
		//b) A partir del diccionari, imprimeix nom�s la llista 
		//d'ingredients.
		
		Iterator<String> it = dic.keySet().iterator();
		while (it.hasNext()){
			System.out.println(it.next());
		}
		
		//c) Imprimeix la quantitat de tom�quets.
		
		System.out.println(dic.get("tom�quets").toString());
		
		//d) Imprimeix aquells ingredients dels quals n'h�gim 
		//de comprar m�s de 3 unitats.
		
		it = dic.keySet().iterator();
		while (it.hasNext()){
			String prod = it.next();
			if (dic.get(prod) > 3){
				System.out.println(prod);
			}
		}
		
		//e) Pregunta a l'usuari un ingredient i retorna el n�mero 
		//d'unitats que s'han de comprar. Si l'ingredient no �s a 
		//la llista de la compra, retorna un missatge d'error.
		
		System.out.println("Introdueix un aliment per mostrar la quantitat:");
		Scanner sc = new Scanner(System.in);
		String ingredient = sc.nextLine();
		
		if (dic.containsKey(ingredient))
			System.out.println("Es necessiten "+dic.get(ingredient)+" "+ingredient);
		else
			System.out.println(ingredient+" no es troba a la llista.");
		
		//f) Imprimeix tota la llista amb aquest format:
		//	ingredient (unitats a comprar)
		
		it = dic.keySet().iterator();
		while (it.hasNext()){
			String prod = it.next();
			System.out.println(prod+" ("+dic.get(prod)+")");
		}		
		
		
		
		//g) A partir del diccionari, sumar totes les 
		//quantitats i donar el n�mero total d'�tems 
		//que hem de comprar. Resultat: 20.
		
		it = dic.keySet().iterator();
		int num = 0;
		while (it.hasNext()){
			String prod = it.next();
			num += dic.get(prod);
		}
		System.out.println("Resultat: "+num);
		
		//h) Pregunta a l'usuari un ingredient, despr�s demana-li un 
		//nou valor d'unitats i modifica l'entrada corresponent al diccionari.
		
		System.out.println("Introdueix un aliment per canviar la quantitat:");
		ingredient = sc.nextLine();
		
		if (dic.containsKey(ingredient)){
			System.out.println("Introdueix la nova quantitat: ");
			num = sc.nextInt();
			dic.put(ingredient, num);
		}	
		else
			System.out.println(ingredient+" no es troba a la llista.");
		
		it = dic.keySet().iterator();
		while (it.hasNext()){
			String prod = it.next();
			System.out.println(prod+" ("+dic.get(prod)+")");
		}
		
		sc.close();
	}
}
