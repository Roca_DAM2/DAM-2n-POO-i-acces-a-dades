## Procediments emmagatzemats

Els SGBD permeten guardar com a part de les dades d'una BD procediments i
funcions que treballin sobre la resta de dades de la BD.

La utilització de procediments emmagatzemats per a realitzar les operacions
més comunes contra la base de dades permet reaprofitar aquests
procediments en diverses aplicacions i diferents llenguatges de programació
que accedeixin a la base de dades.

També fan que sigui més fàcil de modificar les consultes en cas que canvḯ
l'esquema de la base de dades i permeten l'optimització d'algunes consultes.

En aquest apartat veurem com podem cridar des de Java a funcions i procediments
emmagatzemats.

Com que cada sistema utilitza un llenguatge i formats diferents per als
seus procediments emmagatzemats, en Java s'utilitza un conveni unificat,
que després es tradueix al format de crida específic del connector que
estem utilitzant. D'aquesta manera podem cridar procediments de qualsevol
SGBD de la mateixa manera.

### Configuració de MariaDB

Abans de provar la crida de procediments des de Java, anem a veure com podem
saber quins procediments hi ha creats en una BD i com ho hem de fer per
cridar-los nativament.

Per començar, l'usuari que utilitzem per fer la connexió ha de tenir permisos
per executar aquests procediments. En MySQL/MariaDB això es fa donant el
permís *select* sobre la taula *proc* de la BD *mysql*, que és on es guarden
aquests procediments i funcions.

La sentència que otorga aquest permís és:

```
grant select on mysql.proc to <usuari>;
```

Per veure quins procediments i funcions hi ha a la base de dades actual
podem utilitzar:

```
MariaDB [sakila]> show procedure status;
+--------+-------------------+-----------+----------------+---------------------+---------------------+---------------+--------------------------------------------------+----------------------+----------------------+--------------------+
| Db     | Name              | Type      | Definer        | Modified            | Created             | Security_type | Comment                                          | character_set_client | collation_connection | Database Collation |
+--------+-------------------+-----------+----------------+---------------------+---------------------+---------------+--------------------------------------------------+----------------------+----------------------+--------------------+
| sakila | film_in_stock     | PROCEDURE | root@localhost | 2016-01-20 17:30:10 | 2016-01-20 17:30:10 | DEFINER       |                                                  | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
| sakila | film_not_in_stock | PROCEDURE | root@localhost | 2016-01-20 17:30:10 | 2016-01-20 17:30:10 | DEFINER       |                                                  | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
| sakila | rewards_report    | PROCEDURE | root@localhost | 2016-01-20 17:30:10 | 2016-01-20 17:30:10 | DEFINER       | Provides a customizable report on best customers | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
+--------+-------------------+-----------+----------------+---------------------+---------------------+---------------+--------------------------------------------------+----------------------+----------------------+--------------------+
3 rows in set (0.00 sec)

MariaDB [sakila]> show function status;
+--------+----------------------------+----------+----------------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
| Db     | Name                       | Type     | Definer        | Modified            | Created             | Security_type | Comment | character_set_client | collation_connection | Database Collation |
+--------+----------------------------+----------+----------------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
| sakila | get_customer_balance       | FUNCTION | root@localhost | 2016-01-20 17:30:10 | 2016-01-20 17:30:10 | DEFINER       |         | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
| sakila | inventory_held_by_customer | FUNCTION | root@localhost | 2016-01-20 17:30:10 | 2016-01-20 17:30:10 | DEFINER       |         | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
| sakila | inventory_in_stock         | FUNCTION | root@localhost | 2016-01-20 17:30:10 | 2016-01-20 17:30:10 | DEFINER       |         | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
+--------+----------------------------+----------+----------------+---------------------+---------------------+---------------+---------+----------------------+----------------------+--------------------+
3 rows in set (0.00 sec)

```

### Crida a procediments

També podem veure el codi font de qualsevol d'aquestes funcions i procediments:

```
MariaDB [sakila]> show create procedure film_in_stock;
+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+----------------------+--------------------+
| Procedure     | sql_mode                                                                                                                                             | Create Procedure                                                                                                                                                                                                                                                                                                                                       | character_set_client | collation_connection | Database Collation |
+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+----------------------+--------------------+
| film_in_stock | STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION | CREATE DEFINER=`root`@`localhost` PROCEDURE `film_in_stock`(IN p_film_id INT, IN p_store_id INT, OUT p_film_count INT)
    READS SQL DATA
BEGIN
     SELECT inventory_id
     FROM inventory
     WHERE film_id = p_film_id
     AND store_id = p_store_id
     AND inventory_in_stock(inventory_id);

     SELECT FOUND_ROWS() INTO p_film_count;
END | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+----------------------+--------------------+
1 row in set (0.00 sec)
```

En aquest exemple, podem veure que el procediment *film_in_stock* rep tres
paràmetres, tots de tipus enter. Els dos primers són d'entrada (*IN*), és
a dir, s'utilitzen per passar dades cap al procediment. El tercer, en canvi,
és de sortida (*OUT*), així que aquí rebrem algun tipus de resposta després
d'haver cridat el procediment.

Podem veure que el codi del procediment realitza una consulta SQL i, a
l'última línia, es guarda la quantitat de files retornades a aquest tercer
paràmetre.

Cridem el mètode per veure els exemplar de la pel·lícula amb id 4 que hi ha a
la tenda amb id 1:

```
MariaDB [sakila]> call film_in_stock(3,1,@n);
Empty set (0.00 sec)

Query OK, 1 row affected (0.00 sec)

MariaDB [sakila]> call film_in_stock(4,1,@n);
+--------------+
| inventory_id |
+--------------+
|           16 |
|           17 |
|           18 |
|           19 |
+--------------+
4 rows in set (0.02 sec)

Query OK, 1 row affected (0.02 sec)

MariaDB [sakila]> select @n;
+------+
| @n   |
+------+
|    4 |
+------+
1 row in set (0.00 sec)
```

Podem veure que hi ha disponibles els ítems 16, 17, 18 i 19. A la variable
*n* s'ha guardat el total d'ítems disponibles, 4.

### Crida a funcions

Fem el mateix ara amb una funció:

```
MariaDB [sakila]> show create function get_customer_balance;
+----------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+----------------------+--------------------+
| Function             | sql_mode                                                                                                                                             | Create Function                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | character_set_client | collation_connection | Database Collation |
+----------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+----------------------+--------------------+
| get_customer_balance | STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION | CREATE DEFINER=`root`@`localhost` FUNCTION `get_customer_balance`(p_customer_id INT, p_effective_date DATETIME) RETURNS decimal(5,2)
    READS SQL DATA
    DETERMINISTIC
BEGIN








  DECLARE v_rentfees DECIMAL(5,2);
  DECLARE v_overfees INTEGER;      
  DECLARE v_payments DECIMAL(5,2);

  SELECT IFNULL(SUM(film.rental_rate),0) INTO v_rentfees
    FROM film, inventory, rental
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

  SELECT IFNULL(SUM(IF((TO_DAYS(rental.return_date) - TO_DAYS(rental.rental_date)) > film.rental_duration,
        ((TO_DAYS(rental.return_date) - TO_DAYS(rental.rental_date)) - film.rental_duration),0)),0) INTO v_overfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;


  SELECT IFNULL(SUM(payment.amount),0) INTO v_payments
    FROM payment

    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

  RETURN v_rentfees + v_overfees - v_payments;
END | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
+----------------------+------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+----------------------+----------------------+--------------------+
1 row in set (0.00 sec)
```

Les funcions d'usuari es poden cridar com qualsevol altra funció de MySQL.

Per exemple, podem esbrinar quins són els usuaris que ens deuen diners:

```
MariaDB [sakila]> select customer_id, get_customer_balance(customer_id, now()) as balance from customer having balance!=0;
+-------------+---------+
| customer_id | balance |
+-------------+---------+
|         546 |   -3.99 |
|         554 |   -3.00 |
|          16 |   -1.99 |
|         259 |   -1.99 |
|         401 |   -0.99 |
|         577 |   -0.99 |
+-------------+---------+
6 rows in set (0.45 sec)
```

### Crida a procediments des de Java

Per fer la mateixa crida que hem fet abans podríem utilitzar el següent codi:

```java
public class CridaProcedimentEmmagatzemat {
	public static void main(String[] args) {
		String sql = "{call film_in_stock(4,1,?)}";

		try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost/sakila", "root", "usbw");
				CallableStatement st = connection.prepareCall(sql);) {

			st.registerOutParameter(1, Types.INTEGER);

			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt(1));
			}
			System.out.println("Files retornades: "+st.getInt(1));
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
```

Hem utilitzat el mètode *prepareCall()* de la connexió per preparar la crida,
de la mateixa manera que ho fem amb els *PreparedStatement*. En aquest cas,
el mètode ens retorna un *CallableStatement*.

Després hem omplert el paràmetre que hi mancava, indicant que és un
paràmetre de sortida de tipus enter.

Finalment, l'execució del procediment ens retorna un *ResultSet* amb totes les
files obtingudes, i podem recuperar el paràmetre de sortida amb *getInt()*.

També podem generalitzar la consulta, i posar interrogants també pels
paràmetres d'entrada:

```java
public class CridaProcedimentEmmagatzemat {
	public static void main(String[] args) {
		String sql = "{call film_in_stock(?,?,?)}";

		try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost/sakila", "root", "super3");
				CallableStatement st = connection.prepareCall(sql);) {

			st.setInt(1, 4);
			st.setInt(2, 1);
			st.registerOutParameter(3, Types.INTEGER);

			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt(1));
			}
			System.out.println("Files retornades: "+st.getInt(3));
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
```

### Crides a funcions des de Java

La crida de funcions d'usuari no mereix especial atenció, ja que les funcions
es criden de la mateixa manera que qualsevol altra funció de MySQL:

```java
public class CridaFuncio {
	public static void main(String[] args) {
		String sql = "select get_customer_balance(?, now()) as balance";

		try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost/sakila", "root", "usbw");
				PreparedStatement st = connection.prepareStatement(sql)) {
			st.setInt(1, 259);

			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getDouble(1));
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
```
