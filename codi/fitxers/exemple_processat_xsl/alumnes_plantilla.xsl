<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match='/'>
  <html><xsl:apply-templates /></html>
</xsl:template>
<xsl:template match="alumnes">
  <head><title>Llistat d'alumnes</title></head>
  <body>
  <h1>Llista d'alumnes</h1>
  <table border="1">
    <tr><th>Nom</th><th>Edat</th></tr>
      <xsl:apply-templates select="alumne" />
  </table>
  </body>
</xsl:template>
<xsl:template match="alumne">
    <tr><xsl:apply-templates /></tr>
</xsl:template>
<xsl:template match="nom|edat">
    <td><xsl:apply-templates /></td>
</xsl:template>
</xsl:stylesheet>

