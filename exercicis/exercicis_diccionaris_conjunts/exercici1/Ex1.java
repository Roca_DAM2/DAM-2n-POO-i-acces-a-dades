package exercici1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Ex1 {

	public static void main(String[] args) {
		
		Set<String> conjunt = new HashSet<String>();
		Scanner sc = new Scanner(System.in);
		String[] frase;
		
		System.out.println("Introdueix una frase:");
		frase = sc.nextLine().split(" ");
		
		for (int i=0; i<frase.length; i++){
			frase[i]=frase[i].replace(",", "").replace(".", "").toUpperCase();
			
		}
		
		conjunt.addAll(Arrays.asList(frase));
		
		for(String paraula : conjunt){
			int cont = 0;
			for (int i=0; i<frase.length; i++){
				if(paraula.equals(frase[i]))
					cont++;
			}
			System.out.println(paraula+" ("+cont+")");
			
		}
		sc.close();
		

	}

}
