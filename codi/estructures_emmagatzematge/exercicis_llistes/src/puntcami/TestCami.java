package puntcami;

public class TestCami {
	public static void main(String[] args) {
		Cami c = new Cami();
		Punt p;
		
		for (int i=0; i<4; i++) {
			p = new Punt(i, i*2);
			c.afegeixPunt(p);
			System.out.println(p);
		}
		System.out.println(c);
		for (int i=0; i<4; i++) {
			p = new Punt(i+4, i*2);
			c.afegeixPunt(i, p);
			System.out.println(p);
		}
		System.out.println(c);
		System.out.println("Punts: "+c.mida()+", distancia: "+c.distancia());
	}
}
