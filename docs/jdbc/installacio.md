## Instal·lació del MySQL/MariaDB

Per practicar amb JDBC necessitem un SGBD relacional. Farem una instal·lació
local del MySQL o de MariaBD en el nostre entorn de desenvolupament.

El MySQL és un dels SGBD més utilitzats, i és especialment adequat per
aplicacions web que necessiten força rapidesa i no treballen amb un volum
molt gran de dades.

El MariaDB és un *fork* de MySQL amb un model de desenvolupament més obert que
aquest últim, i que proporciona algunes característiques addicionals, bo i
mantenint la compatibilitat amb MySQL.

Tant un com l'altre són programari lliure i són adequats per seguir els
exemples i exercicis d'aquests apunts.

També necessitarem el driver JDBC de connexió a MySQL.

### Instal·lació de MySQL/MariaDB en Debian

1. Instal·lar el paquet *mysql-server* o *mariadb-server*.
2. Quan l'instal·lador ens demani, assignar-li una contrasenya a l'usuari
root del SGBD (no confondre amb l'usuari root del sistema operatiu).

### Instal·lació de MySQL/MariaDB en Windows

Podem utilitzar l'
[instal·lador de MySQL](https://dev.mysql.com/downloads/installer/),
o l'[instal·lador de MariaDB](https://downloads.mariadb.org/mariadb/10.1.10/).

També podem utilitzar paquets integrats com el
[XAMPP](https://www.apachefriends.org/download.html), que inclouen MariaDB,
el servidor web Apache, el PHP i el Perl en un sol instal·lador.

La instal·lació per defecte del *XAMPP* està orientada a entorns de
desenvolupament, així que no s'hauria d'utilitzar directament en entorns de
producció (encara que es pot fer si es revisa la seva configuració).

### Assegurar la installació

Aquests passos són opcionals en un entorn de desenvolupament, però
**imprescindibles** en un entorn de producció.

1. Executar el program *mysql_secure_installation*. Aquest programa assegura
que tenim una contrasenya per root, evita que aquest es pugui connectar des
d'ordinadors remots, i millora alguns altres aspectes de la seguretat bàsica
del SGBD.

2. Crear un usuari sense drets administratius però amb accés complet a les
bases de dades que utilitzem, per evitar connectar-nos al servidor amb
l'usuari root des del nostre programa.

    En entorns de producció es poden crear més usuaris amb permisos
    diferents, per aconseguir que el nostre programa tingui sempre els
    permisos mínims necessaris. Això ens ajuda a protegir les
    dades en cas que el nostre programa tingui una vulnerabilitat o
    s'hagi comès un error.

### Importació de bases de dades d'exemple

En aquest tema utilitzarem algunes BD que es troben disponibles a la web amb
llicències lliures:

- Una [base de dades d'empleats](https://dev.mysql.com/doc/employee/en/).
Aquesta és una base de dades amb poques taules, però molts registres.

Per instal·lar-la s'ha de baixar
[el repositori corresponent a github](https://github.com/datacharmer/test_db).
El més fàcil és baixar-ho clicant a *Download zip*.

Després de descomprimir, podem passar-li l'script que genera la base de dades
al mysql:

```
mysql -u root -p < employees.sql
```

O un cop dins del client mysql:

```
mysql> source employees.sql
```

- [Sakila](https://dev.mysql.com/doc/sakila/en/), una base de dades de lloguer
de pel·lícules.

Aquesta base de dades és molt més completa a nivell de disseny i, a més de
les taules i dades de prova, té vistes i procediments emmagatzemats.

Es pot baixar de la pàgina de
[bases de dades d'exemple del MySQL](https://dev.mysql.com/doc/index-other.html).

Cal carregar primer el fitxer `sakila-schema.sql`, que crea l'estructura de
la BD, i després el fitxer `sakila-data.sql`, que conté les dades.

### Creació d'usuaris

Per crear un usuari de MySQL/MariaDB primer de tot ens hem de connectar com
a root. Aquí ho farem des de línia de comandes, encara que també es pot fer
això des d'entorn gràfic utilitzant programari addicional com el *MySQL
Workbench* o el *PHPMyAdmin*.

Per connectar com a root farem:

```
$ mysql -u root -p
```

Després d'introduir la contrasenya hauríem de veure el *prompt* del SGBD:

```
mysql>
```

A continuació podem crear un usuari amb la sentència SQL *CREATE USER* i
assignar-li permisos amb *GRANT*:

```
CREATE USER 'nom_usuari'@'localhost' IDENTIFIED BY 'contrasenya';
GRANT ALL PRIVILEGES ON nom_BD.* TO 'nom_usuari'@'localhost';
```

Això crearà un usuari que només es podrà connectar des de l'ordinador local
i que tindrà tots els permisos sobre totes les taules de la base de dades
especificada.
