package Ex1;

public class Persona implements Comparable<Persona>{

	private double pes;
	private int edat;
	private int alcada;
	
	public Persona(){
		this(75, 25, 170);
		
	}
	
	public Persona (double pes, int edat, int alcada){
		this.pes = pes;
		this.edat = edat;
		this.alcada = alcada;
		
	}

	
	public int compareTo(Persona p) {
		if (getAlcada() > p.getAlcada())
			return 1;
		else if (getAlcada() < p.getAlcada())
			return -1;
		else 
			return 0;
		
	}

	public double getPes() {
		return pes;
	}

	public void setPes(double pes) {
		this.pes = pes;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getAlcada() {
		return alcada;
	}

	public void setAlcada(int alcada) {
		this.alcada = alcada;
	}
			
	
	
}
