package ex5;

import java.util.Scanner;

public class MainAgenda {
	
	static Scanner sc = new Scanner(System.in);
	static Agenda agenda = new Agenda();

	public static void main(String[] args) {
		
		String opcio = "buit" ;
		
		
		
		while (opcio != "0"){
			System.out.println("\t1.- Introduir contacte.");
			System.out.println("\t2.- Esborrar Contacte.");
			System.out.println("\t3.- Modificar contacte.");
			System.out.println("\t4.- Buscar Contacte.");
			System.out.println("\t5.- Mostra Contactes.");
			System.out.println("\t0.- Sortir.");
			System.out.println("tria una Opcio:");
			opcio = sc.nextLine();
			
			switch (opcio){
				case "1":
					nouContacte();
					break;
				case "2":
					esborraContacte();
					break;
				case "3":
					System.out.println(modificaContacte());
					break;
				case "4":
					System.out.println(buscaContacte());
					break;
				case "5":
					agenda.showContactes();
					break;
				case "0":
					System.out.println("Adeu");
					break;
				default:
					System.out.println("Opci� incorrecta.");
					break;		
			}
		}
		sc.close();
	}
	
	private static void nouContacte(){
		
		System.out.println("Introdueix el nom del contacte:");
		String nom = sc.nextLine();
		System.out.println("Introdueix el Cognom del contacte:");
		String cognom = sc.nextLine();
		System.out.println("Introdueix l'adre�a del contacte:");
		String adreca = sc.nextLine();
		System.out.println("Introdueix el tel�fon del contacte:");
		String telefon = sc.nextLine();
		agenda.addContacte(new Contacte(nom, cognom, adreca, telefon));
	}
	
	private static String esborraContacte(){
		
		System.out.println("(Introdueix el nom del contacte a esborrar:");
		String nom = sc.nextLine();
		if(agenda.removeContacteByNom(nom))
			return "Contacte esborrat";
		else
			return "No s'ha esborrat cap contacte.";
	}
	
	private static Contacte modificaContacte(){
		
		Contacte c = buscaContacte();
		if(c != null){
			String opcio;
			String valor;
			System.out.println("\t1.- Modificar nom.");
			System.out.println("\t2.- Modificar cognom.");
			System.out.println("\t3.- Modificar adre�a.");
			System.out.println("\t4.- Modificar tel�fon.");
			System.out.println("\t0.- Sortir.");
			System.out.println("tria una Opcio:");
			opcio = sc.next();
			
			switch (opcio){
				case "1":
					System.out.println("Introdueix nom nou:");
					break;
				case "2":
					System.out.println("Introdueix cognom nou:");
					break;
				case "3":
					System.out.println("Introdueix adre�a nova:");
					break;
				case "4":
					System.out.println("Introdueix tel�fon nou:");
					break;
			}
			valor = sc.nextLine();
			return agenda.updateContacte(c, opcio, valor);
		}
		return null;
	}
	
	private static Contacte buscaContacte(){
		System.out.println("Introdueix una dada per a trobar un contacte:");
		String valor = sc.nextLine();
		return agenda.findContacte(valor);	
	}
	
	
	
}
