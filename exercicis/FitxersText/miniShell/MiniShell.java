package miniShell;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MiniShell {

	public static void main(String[] args) {
		
		Path dirTreball = Paths.get(System.getProperty("user.dir"));
		Path noudir;
		String[] comanda = new String[2];	
		
		comanda[0] = "";
		Scanner sc = new Scanner(System.in);
		
		
		while(!comanda[0].equals("exit")){
			System.out.print(dirTreball+">");
			comanda = sc.nextLine().split(" ");
			switch (comanda[0]){
			case "ls":
				try (DirectoryStream<Path> stream = Files.newDirectoryStream(dirTreball)) {
					String str;
					for (Path fitxer : stream){
						if (Files.isDirectory(fitxer)){
							str = fitxer+"/";
						} else{
							str = fitxer.toString();
						}
						System.out.println(str);
					}
				} catch (IOException | DirectoryIteratorException ex) {         
        			System.err.println(ex);
        		}
				break;
			case "cd":
				if ( comanda[1] != null){
					noudir = Paths.get(comanda[1]);
					if (noudir.isAbsolute()){
						if (Files.exists(noudir) && Files.isDirectory(noudir))
							dirTreball = Paths.get(comanda[1]);
						else 
							System.out.println("Ruta incorrecta");			
					} else {	
						String[] str = comanda[1].split(Pattern.quote("/"));
						noudir = dirTreball;
						for(String s : str){
							if (s.equals("..")){
								try{
									noudir = noudir.getParent();
									Files.exists(noudir);
								} catch (NullPointerException ex) {
									System.out.println("Estas a l'arrel");
									noudir = dirTreball.getRoot();
								}
							} else {
								try{
									noudir = Paths.get(noudir.toString() + "/"+ s);
								}catch (InvalidPathException ex){
									System.out.println("ruta erronia");
								}
							}
						}
						if(Files.exists(noudir) && Files.isDirectory(noudir)){
							dirTreball = noudir;
						} else {
							System.out.println("ruta erronia");
						}
					}
				} else {
					System.out.println("Utilitzaci� de cd : <cd> <rutaCarpeta>");
				}
					
				
				break;
			case "cat":
				if (comanda[1] != null){
					noudir = Paths.get(dirTreball.toString() + "/"+ comanda[1]);
					if(Files.exists(noudir) && !Files.isDirectory(noudir)){
						try (BufferedReader reader = new BufferedReader(new FileReader(noudir.toString()))){
							String linea;
							while((linea = reader.readLine()) != null){
								System.out.println(linea);
							}
						} catch (IOException e){
							System.err.println("No es troba l'arxiu.");
						}
					} else {
						System.out.println("No es troba el fitxer");
					}
				}else{
					System.err.println("�s: cat fitxer");
				}
				
				break;
			case "exit":
				break;
			default:
				System.out.println("comandes admeses: cat, ls o cd");
				
			}	
		}
		sc.close();
	}

}
