
public class Declinus extends Planta{
	
	private boolean pucCreixer = false;
	private boolean meitatVida = false;
	
	@Override
	Llavor creix(){
		
		if(pucCreixer && !meitatVida){  //Cas en el que pot creixer i no ha arribat a max altura
			altura++;
			if(altura == 4){			
				meitatVida = true;	
			}
			pucCreixer = false;
		}
		else if (pucCreixer && meitatVida){ //Cas en el que pot creixer i ha arribat a max altura 
			altura--;
			if(altura == 0){  //si arriba un altre cop a zero mor
				esViva = false;
			}
			pucCreixer = false;
		}
		else{			// cas en el que no pot creixer
			pucCreixer = true;
		}
		if(altura == 4)   //deixar anar llavors
			return new Llavor(new Declinus());
		
		return null;	
	}
	@Override
	char getChar(int nivell){
		if(altura > nivell)
			return ':';
		else 
			if(altura == nivell)
				return'*';
		else 
			return super.getChar(nivell);
	}

}
