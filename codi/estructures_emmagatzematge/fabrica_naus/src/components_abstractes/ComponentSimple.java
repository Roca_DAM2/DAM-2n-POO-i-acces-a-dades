package components_abstractes;

import java.util.Set;

public abstract class ComponentSimple extends ComponentNau {

	public ComponentSimple(int pes) {
		super(pes);
	}

	@Override
	public boolean add(ComponentNau component) {
		throw new UnsupportedOperationException("No es pot afegir un component a un component simple");
	}

	@Override
	public boolean remove(ComponentNau component) {
		throw new UnsupportedOperationException("No es pot treure un component a un component simple");
	}

	@Override
	public int size() {
		throw new UnsupportedOperationException("No es pot afegir un component a un component simple");
	}

	@Override
	public Set<ComponentNau> getComponents() {
		throw new UnsupportedOperationException("No es pot afegir un component a un component simple");
	}
	
	@Override
	public String descriu() {
		return toString()+"\n";
	}

}
