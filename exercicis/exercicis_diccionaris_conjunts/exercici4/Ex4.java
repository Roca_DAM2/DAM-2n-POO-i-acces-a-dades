package exercici4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		int op = 0;
		String programa = "";
		String versio = "";
		
		Map<String, String> dic = new HashMap<String, String>();
		dic.put("Firefox", "3.5");
		dic.put("Premiere", "CS3");
		dic.put("After Effects", "CS3");
		dic.put("Photoshop", "CS4");
		dic.put("Gimp", "2.0.1");
		dic.put("Python", "2.6.2");
		dic.put("SnagIt", "7.0.1");
		

		Iterator<String> it = dic.keySet().iterator();
		
		while (op != 4){
			System.out.println("1. Mostrar llista");
			System.out.println("2. Consultar versi�");
			System.out.println("3. Afegir Programa");
			System.out.println("4. Sortir");
			System.out.println("Introdueix opci�: ");
			op = sc.nextInt();
			
			switch (op){
			case 1:
				it = dic.keySet().iterator();
				while (it.hasNext()){
					programa = it.next();
					System.out.println(programa+" "+dic.get(programa));
				}
				break;
				
			case 2:
				System.out.println("Introdueix el programa: ");
				programa = sc.next();
				if (dic.containsKey(programa))
					System.out.println("Versio: "+dic.get(programa));
				else
					System.out.println("El programa introudit no existeix.");
				break;
				
			case 3:
				System.out.println("Introdueix el nou programa: ");
				programa = sc.next();
				if (dic.containsKey(programa))
					System.out.println("El programa ja existeix");
				else{
					while (versio.equals("")){
						System.out.println("Introdueix la versi� de  "+programa+":");
						versio = sc.next();
							if (!versio.equals(""))
								dic.put(programa, versio);
								
					}
					versio = "";
				}
				break;
				
			case 4:
				System.out.println("Adeu.");
				break;
				
			default:
				System.out.println("Opci� incorrecta.");
			break;	
			}
		}
		sc.close();
	}
}

