package ex5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class Agenda {
	
	private List<Contacte> agenda = new ArrayList<Contacte>();
	private ListIterator<Contacte> it; 
	
	
	public void addContacte(Contacte c){
		
		agenda.add(c);
		Collections.sort(agenda);
	}
	
	public boolean removeContacteByNom(String nom){
		
		it = agenda.listIterator();
		boolean esborrat = false;
		
		while(!esborrat && it.hasNext()){
			if (it.next().getNom().equalsIgnoreCase(nom)){
				it.remove();
				esborrat = true;
			}
		}
		return esborrat;
	}
	
	public Contacte updateContacte(Contacte c, String opcio, String valor){
		
		switch (opcio){
			case "1":
				c.setNom(valor);
				break;
			case "2":
				c.setCognom(valor);
				break;
			case "3":
				c.setAdreca(valor);
				break;
			case "4":
				c.setTelefon(valor);
				break;
			default:
				break;
		}
		
		Collections.sort(agenda);
		return c;
	}
	
	public Contacte findContacte(String valor){
		
		Contacte contacte = null;
		boolean trobat = false;
		it = agenda.listIterator();
		
		while(!trobat && it.hasNext()){
			contacte = it.next();
			if (contacte.getNom().equalsIgnoreCase(valor)  ){	
				trobat = true;
			}
		}
		if (trobat)
			return contacte;
		else
			return null;
	}
	
	public void showContactes(){
		ListIterator<Contacte> it = agenda.listIterator();
		
		while(it.hasNext()){
			System.out.println(it.next());
		}
	}
}
