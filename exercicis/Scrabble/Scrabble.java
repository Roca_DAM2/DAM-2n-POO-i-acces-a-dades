package scrabble;

public class Scrabble implements Comparable <Scrabble> {
	
	private String paraula;
	private static final String[] lletres = {	"AEILNORSTU",
												"DG",
												"BCMP",
												"FHVWY",
												"K",
												"JX",
												"QZ"};



	public Scrabble(String paraula){
		for(int i = 0; i < paraula.length(); i++){
			if (Character.isLetter(paraula.charAt(i)) == false)
				throw new IllegalArgumentException("Hi ha caracters que no son lletres");
		}
		this.paraula = paraula.toUpperCase();
	}
	
	public int valor(){
		
		char c; 
		int valor = 0;
		
		
		for(int i = 0; i < paraula.length(); i++){
			boolean trobada = false;
			c = paraula.charAt(i);
			for(int j = 0; j < lletres.length; j++){
				if(trobada == false){
					for (int k = 0; k < lletres[j].length();k++){
						if(lletres[j].charAt(k) == c){
							if(j < 5)
								valor += j+1;
							else if(j == 5)
								valor += 8;
							else 
								valor += 10;
							
							trobada = true;
						}	
					}
				}
			}
		}
		return valor;
	}

	@Override
	public int compareTo(Scrabble s) {
		
		return this.valor()-s.valor();
	}
	
	public String toString(){
		return ""+paraula+": "+this.valor()+" punts.";
		
	}
}