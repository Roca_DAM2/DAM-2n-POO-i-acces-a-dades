package jardiV2;

import java.io.Serializable;
import java.util.Random;

/**
 * La classe Planta �s la superclasse per a tots aquells elements del jard�
 * que s�n plantes. La classe Planta treballa conjuntament amb la classe Llavor
 * per aconseguir simular la din�mica de les plantes: les plantes generen llavors
 * i les llavors tornen a generar plantes del mateix tipus.
 * 
 * Aquesta classe defineix comportaments per defecte de les plantes, que poden
 * ser sobreescrits per les classes derivades per adaptar els comportaments de
 * cada esp�cie.
 *
 */
public abstract class Planta implements ElementJardi, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Aquest objecte random es comparteix entre totes les plantes i
	 * permet generar els nombres aleatoris necessaris.
	 */
	private static final Random random = new Random();
	/**
	 * Indica si la planta est� viva o morta. Quan la planta estigui morta
	 * es traur� del jard�.
	 */
	protected boolean viva = true;
	/**
	 * L'al�ada actual de la planta.
	 */
	protected int altura;

	/**
	 * Comportament per defecte de les plantes a mida que passa el temps.
	 * Una planta assoleix un d'al�ada cada vegada que es crida aquest m�tode, fins
	 * a un m�xim de 10. Quan s'intenta sobrepassar aquest m�xim, la planta mor.
	 * 
	 * No es retorna mai cap llavor, sin� que cada subclasse ha de decidir en quines
	 * circumst�ncies ho far�.
	 * 
	 * @return  Retorna sempre null. Fixeu-vos en com �s v�lid posar com a valor de
	 * retorn Llavor en comptes d'ElementJardi. Aix� �s l�gic perqu� una planta nom�s
	 * pot crear llavors, i �s legal a nivell de llenguatge perqu� totes les Llavors
	 * s�n sempre ElementJardi (ja que Llavor implementa ElementJardi).
	 */
	@Override
	public Llavor temps() {
		if (altura < 10)
			altura++;
		else
			viva = false;
		return null;
	}
	
	/**
	 * Per defecte, una planta llan�ar� les seves llavors a dist�ncia 1 o 2, 
	 * cap a la dreta o cap a l'esquerra.
	 * 
	 * @return  Un nombre aleatori entre -2, -1, 1 i 2.
	 */
	@Override
	public int escampa() {
		int signe = 2 * random.nextInt(2) - 1;
		int lloc = random.nextInt(2) + 1;
		return signe * lloc;
	}
	/**
	 * Retorna l'al�ada actual de la planta.
	 * 
	 * @return  l'al�ada actual de la planta.
	 */
	public int getAltura() {
		return altura;
	}

	@Override
	public boolean isViva() {
		return viva;
	}
}